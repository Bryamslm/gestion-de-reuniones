#include <iostream>
#include <queue>
using namespace std;
/*
 * Meetings management:
 *      Luz Clara Mora Salazar
 *      Bryam Lopez Miranda
 *      Estefania Perez Hidalgo
 */

/*
 * Starting date: March 30th, 2021.
 * Final date:  April 28th, 2021.
 */
//------ Prototype section -----
void HardCode();
bool addAdmin(string, int, string);
struct Administrators*searchAdmin(string, int);
struct Teachers*searchTeacher(string, int);
bool addTeacher(string, int, string);
struct Students*searchStudent(string, int);
bool addStudent(string, int, string);
struct Courses*searchCourse(string);
bool addCourse(string, string, int);
void updateTeachersList();
void updateStudentsList();
void print(string);
struct Students;
struct Courses;
struct Administrators;
struct LinkTeacher;
struct LinkStudent;
struct Meeting;
struct SublistAssistance;
bool searchDate(Meeting*meet);
//These variables save the date entered by the user.
int hr=0;
char c;
int mins=0;
char d;
int dy = 0;
int mth = 0;
int yr = 0;
char a;
char b;
string dayyName;
//------ End of the prototype section -----
struct Teachers{
    //List of teachers, attributes: full name, identification, gender.
    string fullName;
    int id;
    string gender;
    Teachers*next;
    Teachers*previous;
    LinkTeacher*link;
    Teachers(string fn, int i, string g){
        fullName=fn;
        id=i;
        gender=g;
        next=NULL;
        previous=NULL;
        link=NULL;
    }
}*firstTeacher;
struct Students{
    //List of students attributes: full name, identification, gender.
    string fullName;
    int id;
    string gender;
    Students*next;
    LinkStudent*link;
    Students(string fn, int i, string g){
        fullName=fn;
        id=i;
        gender=g;
        next=NULL;
        link=NULL;
    }
}*firstStudent;
struct Courses{
    //List of courses, attributes: name, code, credits.
    string name;
    string code;
    int credits;
    Courses*next;
    Courses(string n, string c, int cred) {
        name=n;
        code=c;
        credits=cred;
        next=NULL;
    }
}*firstCourse;
struct Administrators{
    //List of administrators, attributes: full name, identification, g.
    string fullName;
    int id;
    string gender;
    Administrators*next;
    Administrators(string fn, int i, string g){
        fullName=fn;
        id=i;
        gender=g;
        next=NULL;
    }
}*firstAdmin;
struct LinkTeacher{
    //This is a list of links from a teacher to a course, it also contains a sublist called Meeting
    //to take a list of meetings that a teacher has in the course which the link points.
    LinkTeacher*next;
    Courses*course;
    int group;
    Meeting*firstMeeting;
    LinkTeacher(int g) {
        next = NULL;
        course = NULL;
        group = g;
        firstMeeting = NULL;
    }
};
struct LinkStudent{
    //This is the students' link list to a course. It also contains a sublist of attendance linked to the
    //list SubListAssistance to retrieve the attendance list that a student has in the course which the link points.
    LinkStudent*next;
    Courses*course;
    int group;
    SublistAssistance*sublistAssistance;
    LinkStudent(int g){
        next=NULL;
        course=NULL;
        group=g;
        sublistAssistance=NULL;
    }
};
struct SublistAssistance{
    //Attendance sublist belongs to each link in the list of the LinkStudent list, saves the meeting's identification
    // if a student participated in a meeting.
    int id;
    SublistAssistance*next;
    SublistAssistance(int iD){
        id=iD;
        next=NULL;
    }
};
struct Meeting{
    //Meeting sublist belongs to each link in the list of the LinkTeacher list, saves the meeting's identification and
    // the necessary info regarding the exact sate and time (both beginning and end) if a teacher creates in a meeting.
    Meeting*next;
    int id;
    int hour;
    int minute;
    int hourEnd;
    int minuteEnd;
    int day;
    int month;
    int year;
    int weeK;
    string dayName;
    string meetingTitle;
    Meeting(int iD, int hourr, int minutee, int hourEndd, int minuteEndd,int dayy, int monthh, int yearr, int week, string dayname, string title){
        id=iD;
        hour=hourr;
        minute=minutee;
        hourEnd=hourEndd;
        minuteEnd=minuteEndd;
        day=dayy;
        month=monthh;
        year=yearr;
        weeK=week;
        dayName=dayname;
        meetingTitle=title;
        next=NULL;
    }
};
struct MeetingSpecificWeek{
    //This is an independent sublist used in a certain report to storage meetings of an specific week.
    MeetingSpecificWeek*next;
    int id;
    int hour;
    int minute;
    int hourEnd;
    int minuteEnd;
    int day;
    int month;
    int year;
    int weekN;
    string dayName;
    string meetingTitle;
    MeetingSpecificWeek(int iD, int hourr, int minutee, int hourEndd, int minuteEndd,int dayy, int monthh, int yearr, int week, string dayNAME, string title){
        id=iD;
        hour=hourr;
        minute=minutee;
        hourEnd=hourEndd;
        minuteEnd=minuteEndd;
        day=dayy;
        month=monthh;
        year=yearr;
        weekN=week;
        dayName=dayNAME;
        meetingTitle=title;
        next=NULL;
    }
}*firstWeekMeeting;
Administrators*searchAdmin(int id){
    //Searches an administrator to see if it already exists, returns NULL if it is not.
    Administrators*temp=firstAdmin;
    while(temp != NULL){
        if(temp->id==id) {//Search by name.
            cout << "\nThe admin is already part of the administrators\n" << endl;
            return temp;
        }
        temp=temp->next;
    }
    return NULL; //If it returns NULL, the administrators does not exist.
}
bool addAdmin(string fullName, int id, string gender){
    //Function to add administrators to a simple list inserting them at the beginning.
    if(firstAdmin==NULL){
        //If the list of administrators is empty, a new admin is added.
        Administrators*nn = new Administrators(fullName, id, gender);
        firstAdmin=nn;
        return true;
    }else{
        Administrators*admin=searchAdmin(id);
        if(admin != NULL){//Checks if the administrator already exists, if it is NULL it does not exist.
            cout<<"Administrator could not be added because it already exists."<<endl;
            return false;
        }else{
            Administrators*nn= new Administrators(fullName, id, gender);//Creates an administrator.
            nn->next=firstAdmin;
            firstAdmin=nn;  //Inserts at beginning of the simple list.
            return true;
        }
    }
}
Teachers*searchTeacher(int id){//Looks for a teacher to see if it already exists, returns NULL if it does not.
    Teachers*temp=firstTeacher;
    while(temp != NULL){
        if(temp->id==id){//Searches by identification.
            return temp;
        }
        temp=temp->next;
    }
    return NULL; //If the return is NULL, the teacher does not exist.
}
bool addTeacher(string fullName, int id, string gender){
    //Function that adds a teacher to a double list inserting at the beginning.
    if(firstTeacher==NULL){
        //If the teacher list is empty, a new teacher is added first.
        Teachers*nn = new Teachers(fullName, id, gender);
        firstTeacher=nn;
        return true;
    }else{
        Teachers*teacher=searchTeacher(id);//If it returns NULL, the teacher does not exist, but if not then it already exists.
        if(teacher != NULL){//Checks if the teacher exists.
            cout<<"Teacher could not be added because it already exists"<<endl;
            return false;
        }else{
            Teachers*nn= new Teachers(fullName, id, gender);//Creates a teacher.
            nn->next=firstTeacher;
            firstTeacher->previous=nn; //Inserts at the beginning of the double list.
            firstTeacher=nn;
            return true;
        }
    }
}
Students*searchStudent(int id){//Searches for student to see if it already exists, returns NULL if it is not
    Students*temp=firstStudent;
    while(temp != NULL){//Do a cycle while in case there is only one element in the list.
        if(temp->id == id){
            return temp;
        }
        temp=temp->next;
    }
    return NULL;
}
bool addStudent(string fullName, int id, string gender){//Inserts students to a circle list at the beginning.
    if(firstStudent==NULL){//If the list of students is empty, a new student is added.
        Students*nn = new Students(fullName, id, gender);
        firstStudent=nn;
        return true;
    }else{
        Students*student = searchStudent(id);
        if(student != NULL){//If it returns NULL the student does not exist, but if not then it already exists.
            cout<<"Student could not be added"<<endl;
            return false;
        }else{
            Students*nn= new Students(fullName, id, gender);//Creates students.
            Students*temp=firstStudent;
            while(temp != NULL){//Compares the identification of the pointer nn with the previous temp and temp->next.
                if(nn->id >=temp->id &&  temp->next == NULL){
                    //If the pointer nn has a greater identification number than all the others, it is added to the list
                    //asking if the identification of the pointer nn is greater than the identification temp and the next
                    //temp is NULL, the pointer nn would be the highest of identification.
                    temp->next = nn;
                    return true;
                }
                if(nn->id >=temp->id && nn->id <= temp->next->id){
                    //If identification number of the pointer nn is a central element, neither the smallest nor the
                    //largest is added to its corresponding place.
                    nn->next = temp->next;
                    temp->next = nn;
                    return true;
                }
                temp = temp->next;
            }
            if(nn->id <= firstStudent->id){//If it is greater than the first.
                //If the pointer nn is the lowest identification number it is added first and becomes the firstStudent.
                nn->next=firstStudent;
                firstStudent=nn;
                return true;
            }
        }
    }
    return false;
}
Courses*searchCourse(string code){
    //Searches for teacher to see if it already exists, returns NULL if it is not.
    //If the code matches, it returns the course, if it does not find it, returns NULL.
    Courses*temp=firstCourse;
    do{
        if(temp->code == code){
            return temp;
        }
        temp=temp->next;
    } while(temp != firstCourse);
    return NULL;
}
bool addCourse(string name, string code, int credits){//Adds a course to double list inserting at the beginning.
    if(firstCourse==NULL){//If the course list is empty, a new course is created and added.
        Courses*nn = new Courses(name, code, credits);
        firstCourse=nn;
        firstCourse->next=firstCourse; //The circular list is made.
        return true;
    }else{
        Courses*course=searchCourse(code);
        if(course != NULL){///If it returns NULL, the course does not exist, but if not it already exists.
            cout<<"Course could not be added"<<endl;
            return false;
        }else{
            Courses*nn= new Courses(name, code, credits);//create admin
            Courses*temp= firstCourse;
            while(temp->next != firstCourse){//It is added behind the first in the list.
                temp=temp->next;
            }
            temp->next=nn;
            nn->next=firstCourse;
            return true;
        }
    }
}
bool relateTeacherCourse(int idTeacher, string codeCourse, int group){
    Teachers*teacher=searchTeacher(idTeacher);
    //The teacher is search to verify that it exists.
    if(teacher==NULL){
        cout<<"\nThe teacher is not in the list, the link cannot be made"<<endl;
        return false;
    }
    Courses*course=searchCourse(codeCourse);
    //The course is search to verify that it exists.
    if(course==NULL){
        cout<<"\nThe course is not in the list, the link cannot be made"<<endl;
        return false;
    }
    //A new link is created.
    LinkTeacher*nn=new LinkTeacher(group);
    nn->course=course;
    LinkTeacher*temp=teacher->link;
    if(temp==NULL){
        //If the list of links is empty it is only assigned directly.
        teacher->link=nn;
        return true;
    }else{
       do{
            /*
             * This cycle at the same time that it seeks to position itself in the last position of the list to add the,
             *new course to the teacher also checks that the course and group do not repeatedly get into the
             *same teacher, that's why it's a do while loop.
            */
            if(temp->course->code == nn->course->code && temp->group == nn->group){
                cout<<"This teacher has already assigned this group and course"<<endl;
                return false;
            }
            if(temp->next==NULL){
                break;
            }
            temp=temp->next;
        } while(temp != NULL);
        //Insert the new link in the list.
        temp->next=nn;
        return true;
    }
}
bool relateStudentCourse(int idStudent, string codeCourse, int group){
    Students*student=searchStudent(idStudent);//Student wanted on list.
    if(student==NULL){
        cout<<"\nThe student is not in the list, the link cannot be made."<<endl;
        return false;
    }
    Courses*course=searchCourse(codeCourse);//Searches for course in list.
    if(course==NULL){
        cout<<"\nThe course is not in the list, the link cannot be made."<<endl;
        return false;
    }
    bool courseFoud=false;//Variable to check if the course is being taught.
    Teachers*tempT=firstTeacher;
    while(tempT != NULL){
        /*
         * This cycle checks that the course is being taught by a teacher in order to assign it to a student
         * if the course is being taught, the courseFound variable changes to true, if it does not change when,
         * everything is finished the loop while the course is not being taught, even though it is in the course list.
         */
        LinkTeacher*tempL=tempT->link;
        while(tempL != NULL){
            //Scroll link list.
            if(tempL->course == course && tempL->group == group){
                //The link was found.
                courseFoud=true;
                break;
            }
            tempL=tempL->next;
        }
        if(courseFoud){
            //If the link was found, the cycle is exited.
            break;
        }
        tempT=tempT->next;
    }
    if(courseFoud==false){
        //If the link was not found.
        cout<<"\nThe course "<<course->name<<" group"<<group<<" cannot be added because no teacher is teaching it."<<endl;
        return false;
    }
    //New link is created.
    LinkStudent*nn=new LinkStudent(group);
    nn->course=course;
    LinkStudent*temp=student->link;
    if(temp==NULL){
        //If the list of links is empty, only the new link is assigned directly.
        student->link=nn;
        return true;
    }else{
        do{
            /*
             * This cycle at the same time that it seeks to position itself in the last position of the list to add the
             * new course to the student also checks that the course and group do not repeatedly get into the,
             * same student, that's why it's a do while loop.
            */
            if(temp->course->code == nn->course->code && temp->group == nn->group){
                cout<<"This student has already assigned this group and course"<<endl;
                return false;
            }
            if(temp->next==NULL){
                break;
            }
            temp=temp->next;
        } while(temp != NULL);
        //The new link to the list is entered.
        temp->next=nn;
        return true;
    }
}
bool searchiDMeeting(int idMeeting){
    Teachers*tempTeacher= firstTeacher;
    while(tempTeacher != NULL){
        //Cycle to find that ID is not assigned to another meeting.
        //This cycle goes through each teacher on the list.
        LinkTeacher*tempLink=tempTeacher->link;
        while(tempLink != NULL){
            //This cycle goes through each link to the teacher's course.
            Meeting*tempMeeting=tempLink->firstMeeting;
            while(tempMeeting != NULL){
                //This cycle runs through each meeting of each teacher course link.
                if(tempMeeting->id == idMeeting){
                    //If the ID of the meeting is equal to the ID that you want to use.
                    //It is a restriction therefore it returns false, the meeting is not assigned.
                    cout<<"\nThere is another meeting with this ID assigned."<<endl;
                    return false;
                }
                tempMeeting=tempMeeting->next;
            }
            tempLink=tempLink->next;
        }
        tempTeacher=tempTeacher->next;
    }
    return true;
}
bool meetingTeacher(int idTeacher, string codeCourse,int group, int idMeeting, int hour, int minute, int hourE, int minuteE,int day, int month, int year, int week, string dayname, string titleMeeting){
    if(hourE< hour){
        cout<<"\nThe end time must be greater than the start time."<<endl;
        return false;
    }else if(hourE== hour && minuteE < minute){
        cout<<"\nThe end time must be greater than the start time."<<endl;
        return false;
    }
    Teachers*teacher=searchTeacher(idTeacher);//if it returns NULL, the teacher does not exist, but it already exists
    if(teacher == NULL){//Teacher does not exist.
        cout<<"Teacher not found."<<endl;
        return false;
    }
    bool idFound=searchiDMeeting(idMeeting);
    if(idFound==false){
        return false;
    }
    LinkTeacher*temp= teacher->link;
    while(temp != NULL){
         //This cycle goes through each course link in search of the course where the meeting should be assigned, otherwise it finds.
         //The course link is because the course is not being taught, therefore the meeting cannot be assigned.

        if(temp->course->code == codeCourse && temp->group==group){

            Meeting*nn= new Meeting(idMeeting, hour, minute, hourE, minuteE, day, month, year,week,dayname, titleMeeting);
            if(temp->firstMeeting==NULL){
                temp->firstMeeting=nn;
                return true;
            }else{
                Meeting*temp2=temp->firstMeeting;
               do{
                   //This cycle, apart from going through the course links, verifies that the teacher has no other.
                   //Meeting at the same time, the same day, month and year.
                   //Check that a meeting is not scheduled at the same time as another.
                    if(temp2->year==nn->year && temp2->month == nn->month && temp2->day==nn->day && temp2->hour==nn->hour && temp2->minute==nn->minute){
                        cout<<"\nThe meeting cannot be assigned because this teacher already has a meeting at the same"<<
                        " time, day, month and year."<<endl;
                        return false;
                    }
                    //check that a meeting is not scheduled when another is in progress
                   if(temp2->year==nn->year && temp2->month == nn->month && temp2->day==nn->day && temp2->hourEnd > nn->hour){
                       cout<<"\nYou cannot schedule the meeting because, you will be in another meeting at that time."<<endl;
                       return false;
                   }
                   //check that a meeting is not scheduled when another is in progress
                   if(temp2->year==nn->year && temp2->month == nn->month && temp2->day==nn->day && temp2->hourEnd == nn->hour && temp2->minuteEnd > nn->minute){
                       cout<<"\nYou cannot schedule the meeting because, you will be in another meeting at that time."<<endl;
                       return false;
                   }
                    if(temp2->next==NULL){
                        break;
                    }
                    temp2=temp2->next;
                }while(temp2 != NULL);
                //The meeting is added to the list.
                temp2->next=nn;
                return true;
            }
        }
        temp=temp->next;
    }
    cout<<"The meeting cannot be assigned to this course because the teacher does not teach the course."<<endl;
    return false;
}
bool meetingStudent(int idStudent, string codeCourse, int courseGroup, int idMeeting){
    Students*student=searchStudent(idStudent);//Student is search.
    if(student==NULL){
        cout<<"\nStudent not found."<<endl;
        return false;
    }
    LinkStudent*temp=student->link;
    while(temp != NULL){
        //This cycle looks for the link that the course and group have to register attendance.
        if(temp->course->code==codeCourse && temp->group==courseGroup){
            Teachers*temp2=firstTeacher;
            while(temp2 != NULL){
                //This cycle goes through the list of teachers.
                LinkTeacher*temp3=temp2->link;
                while(temp3 != NULL){
                    //This loop cycles through the teacher link list.
                    Meeting*temp4=temp3->firstMeeting;
                    while(temp4 != NULL){
                        //This cycle goes through the list of meetings that is in each link of each teacher.
                        if(temp4->id==idMeeting){
                            //If the meeting id is the same as the one passed by parameter, yes it was attended.
                            bool verifyDate= searchDate(temp4);
                            if(verifyDate==true) {
                                cout << "Attendance cannot be added because the meeting has not passed yet." << endl;
                                return false;
                            }
                            SublistAssistance* nn=new SublistAssistance(idMeeting);
                            //Create new link.
                            if(temp->sublistAssistance==NULL){
                                //If the link list is empty then the new link is added directly.
                                temp->sublistAssistance=nn;
                                return true;
                            }else{
                                SublistAssistance*temp5=temp->sublistAssistance;
                                //It is positioned at the end of the list to add the new link.
                                while(temp5->next != NULL){
                                    if(temp5->id==nn->id){
                                        cout<<"\nYou have already registered attendance at this meeting."<<endl;
                                        return false;
                                    }
                                    temp5=temp5->next;
                                }
                                temp5->next=nn;
                                return true;
                            }
                        }
                        temp4=temp4->next;
                    }
                    temp3=temp3->next;
                }
                temp2=temp2->next;
            }
        }
        temp=temp->next;
    }
    //The meeting id was not found therefore it was not participated.
    cout<<"\nThe student did not attend the meeting"<<endl;
    return false;
}

void modifyTeacherWoCourses(int id){ //Modifies a professor without an assigned course.
    //Function that modify teacher without course.
    Teachers*teacher = searchTeacher(id);
    if(teacher==NULL){
        cout<<"\nTeacher not found"<<endl;
        return;
    }
    if (teacher->link==NULL){
        string option;
        cout<<"\nThis is the information of this teacher:"<<endl;
        cout<<"Full name: "<<teacher->fullName<<"\nGender: "<<teacher->gender<<"\nId: "<<teacher->id<<endl;
        cout<<"What do you want to modify?\n1- Full name\n2- Gender\n3- ID"<<endl;
        cout<<"Select an option: ";
        cin>>option;
        //The user enters the action to execute.
        if(option=="1"){
            string name;
            cout<<"\nWrite new full name: ";
            cin>>name;
            teacher->fullName=name;//Name is modified.
            cout<<"\nModification successful!"<<endl;
            return;
        }else if(option=="2"){
            string gender;
            cout<<"\nWrite new gender: ";
            cin>>gender;
            teacher->gender=gender;//Gender is modified.
            cout<<"\nModification successful!"<<endl;
            return;
        }else if(option=="3"){
            int newId;
            while(true) {
                //While the ID is not valid, it will be requested.
                cout << "\nWrite new ID (only numbers): ";
                cin >> newId;
                Teachers *teacher2 = searchTeacher(newId);
                if (teacher2 == NULL) {
                    teacher->id=newId;//ID is modified.
                    cout<<"\nModification successful!"<<endl;
                    return;
                }
                cout << "\nThis ID is not available, please try another." << endl;
            }
        }
    }
    else{
        cout<<"The professor already has courses assigned.";
        return;
    }
}
void modifyTeacherWithCourses(int id){ //Modifies a professor without an assigned course.
    //Function that modify teacher with course.
    Teachers*teacher = searchTeacher(id);
    if(teacher==NULL){
        cout<<"\nTeacher not found"<<endl;
        return;
    }
    if (teacher->link!=NULL){
        string option;
        cout<<"\nThis is the information of this teacher:"<<endl;
        cout<<"Full name: "<<teacher->fullName<<"\nGender: "<<teacher->gender<<"\nId: "<<teacher->id<<endl;
        cout<<"What do you want to modify?\n1- Full name\n2- Gender\n3- ID"<<endl;
        cout<<"Select an option: ";
        cin>>option;
        //The user enters the action to execute.
        if(option=="1"){
            string name;
            cout<<"\nWrite new full name: ";
            cin>>name;
            teacher->fullName=name;//Full name is modified.
            cout<<"\nModification successful!"<<endl;
            return;
        }else if(option=="2"){
            string gender;
            cout<<"\nWrite new gender: ";
            cin>>gender;
            teacher->gender=gender;//Gender is modified.
            cout<<"\nModification successful!"<<endl;
            return;
        }else if(option=="3"){
            int newId;
            while(true) {
                //While the ID is not valid, it will be requested.
                cout << "\nWrite new ID (only numbers): ";
                cin >> newId;
                Teachers *teacher2 = searchTeacher(newId);
                if (teacher2 == NULL) {
                    teacher->id=newId;//ID is modified
                    cout<<"\nModification successful!"<<endl;
                    return;
                }
                cout << "\nThis ID is not available, please try another." << endl;
            }
        }
    }
    else{
        cout<<"The professor already has no courses assigned.";
        return;
    }

}
void deleteLinkCourseStudent(string code, int group=0){
    //Function that deletes course to students.
    Students*temp=firstStudent;
    while(temp != NULL){
        //scroll student list.
        LinkStudent*tempLink=temp->link;

        if(tempLink==NULL){
            temp = temp->next;
            continue;
        }
        //If the course to be deleted is identified, it stops pointing, making the one who points it point to the next.
        else if((tempLink->course->code == code && tempLink->group == group) || (tempLink->course->code == code && group==0)){
            temp->link=temp->link->next;
        }else {
            tempLink = tempLink->next;
            LinkStudent *temp2 = temp->link;
            //The list is scrolled to identify the course to delete.
            while (tempLink != NULL) {
                //If the course to be deleted is identified, it stops pointing, making the one who points it point to the next.
                if ((tempLink->course->code == code && tempLink->group == group)||(tempLink->course->code == code && group==0)){
                    temp2->next=tempLink->next;
                    break;
                }
                temp2=temp2->next;
                tempLink=tempLink->next;
            }
        }
        temp = temp->next;
    }
}
bool deleteTeacherWoCourses(int id){
    //Function that removes teacher without course.
    Teachers*teacher= searchTeacher(id);
    if(teacher==NULL) {
        cout << "Teacher not found" << endl;
        return false;
    }
    if (teacher->link==NULL) {
        if (firstTeacher == teacher) {
            // If the one to be deleted is the first one in the list, only the next one in the previous ones is made to point to NULL,
            // and redefine the firstTeacher as the second in the list.
            Teachers *temp = firstTeacher->next;
            temp->previous = NULL;
            firstTeacher = temp;
            cout << "\nDelete successful!" << endl;
            return true;
        } else if (teacher->next == NULL) {
            //If the one to be deleted is the last one in the list, only the one that is penultimate is made to point to NULL.
            Teachers *temp2 = teacher->previous;
            temp2->next = NULL;
            cout << "\nDelete successful! Center." << endl;
            return true;
        } else {
            // If the one to be deleted has previous and next different from NULL, only the previous one is made,
            // point in next to which the one to be deleted in next is pointing and that next in previous point,
            // to the one that is before the one to be deleted, basically it stops pointing to the one to be deleted.
            Teachers *temp3 = teacher->previous;
            Teachers *temp4 = teacher->next;
            temp3->next = temp4;
            temp4->previous = temp3;
            cout << "\nDelete successful! first." << endl;
            return true;
        }
    }
    else{
        cout<<"The professor already has courses assigned.";
        return false;
    }
}
bool deleteTeacherWithCourses(int id){
    //Function that removes teacher with course.
    Teachers*teacher= searchTeacher(id);//search teacher.
    if(teacher==NULL) {
        cout << "Teacher not found." << endl;
        return false;
    }
    if (teacher->link!=NULL) {
        LinkTeacher*courses=teacher->link;
        while(courses != NULL){
            // Cycle that calls function to delete student course taught by the teacher to delete.
            deleteLinkCourseStudent(courses->course->code, courses->group);
            courses=courses->next;
        }
        if(teacher->next==NULL && teacher->previous==NULL){
            firstTeacher=NULL;
            cout<<"\nDeleted successfully!"<<endl;
            return true;
        }else if(firstTeacher==teacher){
            // If the one to be deleted is the first one in the list, only the next one in the previous ones is made to point to NULL,
            // and redefine the firstTeacher as the second in the list.
            Teachers*temp=firstTeacher->next;
            temp->previous=NULL;
            firstTeacher=temp;
            cout<<"\nDeleted successfully!"<<endl;
            return true;
        } else if(teacher->next==NULL){
            // If the one to be deleted is the last one in the list, only the one that is penultimate is made to point to NULL.
            Teachers*temp2=teacher->previous;
            temp2->next=NULL;
            cout<<"\nDeleted successfully!"<<endl;
            return true;
        }else{
            // If the one to be deleted has previous and next different from NULL, only the previous one is made,
            // point in next to which the one to be deleted in next is pointing and that next in previous point,
            // to the one that is before the one to be deleted, basically it stops pointing to the one to be deleted.
            Teachers*temp3=teacher->previous;
            Teachers*temp4=teacher->next;
            temp3->next=temp4;
            temp4->previous=temp3;
            cout<<"\nDeleted successfully!"<<endl;
            return true;
        }
    }
    else{
        cout<<"The professor does not have courses assigned.";
        return false;
    }
}
void modifyStudentWoCourses(int id){
    //Function that allows modifying full name,Gender or ID of a student.
    Students*student= searchStudent(id);//search student.
    if(student==NULL){
        cout<<"\nTeacher not found."<<endl;
        return;
    }
    if (student->link==NULL){
        string option;
        cout<<"\nThis is the information of this student:"<<endl;
        cout<<"Full name: "<<student->fullName<<"\nGender: "<<student->gender<<"\nId: "<<student->id<<endl;
        cout<<"What do you want to modify?\n1- Full name\n2- Gender\n3- ID"<<endl;
        cout<<"Select an option: ";
        cin>>option;
        //The user enters the action to execute.
        if(option=="1"){
            string name;
            cout<<"\nWrite new full name: ";
            cin>>name;
            student->fullName=name;//FUll name is modified.
            cout<<"\nModification successful!"<<endl;
            return;
        }else if(option=="2"){
            string gender;
            cout<<"\nWrite new gender: ";
            cin>>gender;

            student->gender=gender;//Gender modified

            cout<<"\nModification successful!"<<endl;

            return;
        }else if(option=="3"){

            int newId;

            while(true) {
                //While the ID is not valid, it will be requested.
                cout << "\nWrite new ID (only numbers): ";
                cin >> newId;
                Teachers *teacher2 = searchTeacher(newId);
                if (teacher2 == NULL) {
                    student->id=newId;//ID is modified
                    cout<<"\nModification successful!"<<endl;
                    return;
                }
                cout << "\nThis ID is not available, please try another." << endl;
            }
        }
    }
    else{
        cout<<"The student already has courses assigned.";
        return;
    }
}
void modifyStudentWithCourses(int id){
    //Function that allows modifying full name,Gender or ID of a student.
    Students*student= searchStudent(id);//Search student.
    if(student==NULL){
        cout<<"\nTeacher not found"<<endl;
        return;
    }
    if (student->link!=NULL){
        string option;
        cout<<"\nThis is the information of this student:"<<endl;
        cout<<"Full name: "<<student->fullName<<"\nGender: "<<student->gender<<"\nId: "<<student->id<<endl;
        cout<<"What do you want to modify?\n1- Full name\n2- Gender\n3- ID"<<endl;
        cout<<"Select an option: ";
        cin>>option;
        //The user enters the action to execute.
        if(option=="1"){
            string name;
            cout<<"\nWrite new full name: ";
            cin>>name;
            student->fullName=name;//FUll name is modified.
            cout<<"\nModification successful!"<<endl;
            return;
        }else if(option=="2"){
            string gender;
            cout<<"\nWrite new gender: ";
            cin>>gender;
            student->gender=gender;//Gender modified.
            cout<<"\nModification successful!"<<endl;
            return;
        }else if(option=="3"){
            int newId;
            while(true) {
                //While the ID is not valid, it will be requested.
                cout << "\nWrite new ID (only numbers): ";
                cin >> newId;
                Teachers *teacher2 = searchTeacher(newId);
                if (teacher2 == NULL) {
                    student->id=newId;//ID is modified.
                    cout<<"\nModification successful!"<<endl;
                    return;
                }
                cout << "\nThis ID is not available, please try another." << endl;
            }
        }
    }
    else{
        cout<<"The student does not have courses assigned.";
        return;
    }
}
void modifyCourse(string code){
    //Function that allows modifying name, code or credits of a course.
    Courses*course= searchCourse(code);//Search course.
    if(course==NULL){
        cout<<"\nTCourse not found."<<endl;
        return;
    }
    string option;
    cout<<"\nThis is the information of this course:"<<endl;
    cout<<"Name: "<<course->name<<"\nCredits: "<<course->credits<<"\nCode: "<<course->code<<endl;
    cout<<"What do you want to modify?\n1- Name\n2- Credits\n3- Code"<<endl;
    cout<<"Select an option: ";
    cin>>option;
    //The user enters the action to execute.
    if(option=="1"){
        string name;
        cout<<"\nWrite new name: ";
        cin>>name;
        course->name=name;//Name is modified.
        cout<<"\nModification successful!"<<endl;
        return;
    }else if(option=="2"){
        int credits;
        cout<<"\nWrite new credits number: ";
        cin>>credits;
        course->credits=credits;//Credits are modified.
        cout<<"\nModification successful!"<<endl;
        return;
    }else if(option=="3"){
        string code;
        while(true) {
            //While the ID is not valid, it will be requested.
            cout << "\nWrite new code: ";
            cin >> code;
            Courses*temp = searchCourse(code);//Search course.
            if(temp == NULL) {
                course->code=code;//ID is modified.
                cout<<"\nModification successful!"<<endl;
                return;
            }
            cout << "\nThis code is not available, please try another." << endl;
        }
    }
}
bool deleteStudentWithCourses(int id){
    //Function that removes student with course.
    Students*student=searchStudent(id);//Search student.
    if(student==NULL){
        cout << "Student not found." << endl;
        return false;
    }
    if (student->link!=NULL){//Meet the requirement to have courses.
        if(student==firstStudent){//if is the first.
            firstStudent=firstStudent->next;
            cout<<"\nDeleted successfully!"<<endl;
            return true;
        }

        Students*temp=firstStudent;

        while(temp->next != student){
            //Search the correct student for remove.
            temp=temp->next;
        }
        //The student is ignore.
        temp->next=student->next;

        cout<<"\nDeleted successfully!"<<endl;
        return true;
    }
    else{//Does not meet the requirement of having courses.
        cout << "The student does not have courses assigned.";
        return false;
    }
}
bool deleteStudentWoCourses(int id){
    //Function that removes student without course.
    Students*student=searchStudent(id);//Search student.
    if(student==NULL){
        cout << "Student not found." << endl;
        return false;
    }
    if (student->link==NULL){//Meet the requirement to have courses.
        if(student==firstStudent){//if is the first.
            firstStudent=firstStudent->next;
            cout<<"\nDeleted successfully!"<<endl;
            return true;
        }
        Students*temp=firstStudent;
        while(temp->next != student){
            //Search the correct student for remove.
            temp=temp->next;
        }
        //The student is ignore.
        temp->next=student->next;
        cout<<"\nDeleted successfully!"<<endl;
        return true;
    }
    else {
        cout << "The student does not have courses assigned.";
        return false;
    }
}
void deleteLinkCourseTeacher(string code){
    //Function that removes a course from the teacher list.
    Teachers*temp=firstTeacher;

    while (temp != NULL){
        //Scroll the list teacher.
        LinkTeacher*temp2=temp->link;

        if(temp2==NULL){
            //If links list is NULL.
            temp=temp->next;
            continue;
        }else if(temp2->course->code == code){
            //If the first link in the list must be removed.
            temp->link=temp2->next;
            return;
        }else{
            LinkTeacher*temp3=temp->link;
            temp2=temp2->next;

            while(temp2 != NULL){
                //Scroll list links.
                if(temp2->course->code==code){
                    //The course to be removed is ignored.
                    temp3->next=temp2->next;
                    return;
                }
                temp2=temp2->next;
                temp3=temp3->next;
            }
        }
        temp=temp->next;
    }
}
bool deleteCourse(string code){
    //Function that removes a course, it is deleted both from the list,
    //of courses and from teachers and students who have it assigned.
    Courses*course= searchCourse(code);
    if(course==NULL){
        cout<<"\nCourse not found"<<endl;
        return false;
    }
    //Delete course in teacher.
    deleteLinkCourseTeacher(course->code);
    //Delete course in student.
    deleteLinkCourseStudent(course->code);
    Courses*temp=firstCourse;
    //Delete course in list courses.
    while (temp->next != course){
        temp=temp->next;
    }
    //The course to be deleted is stopped pointing.
    temp->next=course->next;

    if(course=firstCourse){
        firstCourse=course->next;
    }
    cout<<"\nDelete successful!"<<endl;
    return true;
}
bool searchDate(Meeting*meet){
    //Returns false if reunion has already passed and returns true if it hasn't.
    //Compare the date of the meeting with the date entered by the user when starting the program.
    if(meet->month > mth)
        return true;
    if(yr==meet->year && meet->month < mth)
        return false;
    if(mth==meet->month && meet->day> dy)
        return true;
    if(mth==meet->month && meet->day < dy)
        return false;
    if(dy==meet->day && meet->hour> hr)
        return true;
    if(dy==meet->day && meet->hour< hr)
        return false;
    if(hr==meet->hour && meet->minute > mins)
        return true;
    return false;
}
void deleteMeetingOfTeacher(int idTeacher,string codeCourse, int groupCourse){
    //Function that eliminates a teacher meeting.
    Teachers*teacher=searchTeacher(idTeacher);//search teacher.
    if(teacher== NULL){
        cout<<"Teacher not found."<<endl;
        return;
    }
    LinkTeacher*linkTeacher=teacher->link;

    if(linkTeacher == NULL){
        cout<<"\nThis teacher has not asignnated courses."<<endl;
        return;
    }else {
        while (linkTeacher != NULL) {
            //Scroll the links teacher search the correct link.
            if (linkTeacher->course->code == codeCourse && linkTeacher->group == groupCourse) {
                break;
            }
            linkTeacher = linkTeacher->next;
        }
        if (linkTeacher == NULL) {
            //If linkTeacher is NULL the teacher has no asignnated that course.
            cout << "\nThis teacher has no asignnated that course." << endl;
            return;
        } else {
            Meeting *meet = linkTeacher->firstMeeting;
            if (meet == NULL) {
                //If meet is NULL there aren't meetings programmed.
                cout << "\nThis teacher has no meetings programmed for this course." << endl;
                return;
            }
            cout << "\nThis are the meetings programmed for this course." << endl;
            bool flag = false;//If the teacher does have at least one future meeting, it changes to true.
            while (meet != NULL) {
                //Scroll the links teacher search the correct link.
                bool printMeeting = searchDate(meet);//Search if meetings is future, returns true if it is.
                if (printMeeting == true) {
                    //if meetings is future
                    flag = true;//The teacher does have one meeting future.
                    cout << "\nID: " << meet->id << "\nTitle: " << meet->meetingTitle << "\nStart date: " << meet->day
                         << "/" << meet->month << "/" << meet->year << "\nStart hour: " << meet->hour << ":"
                         << meet->minute <<
                         "\nFinish hour: " << meet->hourEnd << ":" << meet->minuteEnd << endl;
                }
                meet = meet->next;
            }
            if (flag == false) {
                //If flag is false it is because there was no future meeting.
                cout << "This teacher has no future meetings for this course." << endl;
                return;
            }
            int option;
            cout << "\nEnter the ID of the meeting to delete: ";

            cin >> option;

            Meeting*meet2=linkTeacher->firstMeeting;

            if(meet2->id==option){
                //If the meeting is the first in the list.
                linkTeacher->firstMeeting=meet2->next;
                cout<<"\nMeeting deleted successfully."<<endl;
                return;
            }
            Meeting*meet3;
            while(meet2->id != option){
                //Scroll the list search the correct meeting.
                meet3=meet2;
                meet2=meet2->next;
            }
            meet3->next=meet2->next;

            cout<<"\nMeeting deleted successfully."<<endl;

            return;
        }
    }
}
void modifyMeetingOfTeacher(int id, string code, int group){
    //Modify a teacher's meeting.
    Teachers*teacher=searchTeacher(id);
    //Search teacher
    if(teacher== NULL){
        cout<<"\nTeacher not found"<<endl;
        return;
    }
    LinkTeacher*linkTeacher=teacher->link;
    if(linkTeacher == NULL){
        cout<<"\nThis teacher has not asignnated courses"<<endl;
        return;
    }else{
        while(linkTeacher != NULL){
            //The list of links is scrolled.
            if(linkTeacher->course->code==code && linkTeacher->group==group){
                //Required link is searched.
                break;
            }
            linkTeacher=linkTeacher->next;
        }
        if(linkTeacher==NULL){
            //The required link was not found
            cout<<"\nThis teacher has no asignnated that course"<<endl;
            return;
        }else{
            Meeting*meet=linkTeacher->firstMeeting;
            if(meet==NULL){
                //No meetings
                cout<<"\nThis teacher has no meetings programmed for this course"<<endl;
                return;
            }
            cout<<"\nThis are the meetings programmed for this course"<<endl;
            bool flag=false;//To knows if the teacher has meetings held or not.
            while(meet != NULL){
                bool printMeeting=searchDate(meet);
                if(printMeeting==true) {
                    flag=true;//the teacher has meetings held
                    cout << "\nID: " << meet->id << "\nTitle: " << meet->meetingTitle << "\nStart date: " << meet->day
                    << "/" <<meet->month << "/" << meet->year << "\nStart hour: " << meet->hour << ":" << meet->minute <<
                    "\nFinish hour: " << meet->hourEnd << ":" << meet->minuteEnd <<"\nweek: "<<meet->weeK<<"\nDay: "<<meet->dayName<<endl;
                }
                meet=meet->next;
            }
            if(flag==false){
                //the teacher has not meetings held
                cout<<"This teacher has no future meetings for this course"<<endl;
                return;
            }
            int option;
            cout<<"\nEnter the ID of the meeting to modify: ";
            cin>>option;
            Meeting*meet2=linkTeacher->firstMeeting;
            while(meet2->id != option){
                //The required meeting is sought
                meet2=meet2->next;
            }
            if(meet2==NULL){
                cout<<"\nMeeting not found"<<endl;
                return;
            }
            cout<<"\nWhat do you want to modify?\n1- ID\n2- Title\n3- Date\n\nSelect an option: ";
            string option2;
            cin>>option2;
            if(option2=="1"){
                while(true) {
                    cout << "\nWrite the new ID: ";
                    cin>>option;
                    if(searchiDMeeting(option)==false){
                        //Wanted that ID is available
                        cout<<"\nThis ID is not available, try another"<<endl;
                    }else{
                        meet2->id=option;
                        cout<<"\nSuccessful modification"<<endl;
                        return;
                    }
                }
            }else if(option2=="2"){
                string newTitle;
                cout<<"\nWrite the new title: ";
                cin.ignore();
                getline(cin, newTitle);
                meet2->meetingTitle=newTitle;
                //Title is modified
                cout<<"\nSuccessful modification"<<endl;
                return;
            }else if(option2=="3"){
                while(true) {
                    //These variables are required to enter the new date
                    bool invalide=true;
                    int hour = 0;
                    char c;
                    int minutes = 0;
                    char d;
                    int day = 0;
                    int mounth = 0;
                    int year = 0;
                    char a;
                    char b;
                    int week;
                    string dayName;
                    cout << "\nWrite de new date (for examanple: 15:20/29/03/2021): ";
                    cin >> hour >> c >> minutes >> d >> day >> a >> mounth >> b >> year;
                    if (hour >= 0 && hour <= 23 && c == ':' && minutes >= 0 && minutes <= 59 && d == '/' && day > 0 &&
                        day < 32 && a == '/' && mounth > 0 && mounth < 13 && b == '/' && year > 2020 && year < 2022) {
                        //It is verified that the date entered is valid
                        int hourE;
                        int minuteE;
                        cout << "\nWrite de new final hour meeting (for examanple: 15:20): ";
                        cin >> hourE >> c >> minuteE;
                        cout << "\nWrite de new week: ";
                        cin >> week;
                        cout << "\nWrite de new day name: ";
                        cin >> dayName;
                        LinkTeacher*temp=teacher->link;
                        while(temp != NULL) {
                            //Links list is scrolled
                            Meeting*temp2=temp->firstMeeting;
                            while (temp2 != NULL) {
                                /*
                                * This cycle, apart from going through the course links, verifies that the teacher has no other
                                * meeting at the same time, the same day, month and year
                                */
                                // check that a meeting is not scheduled at the same time as another
                                if (temp2->year == year && temp2->month == mounth && temp2->day == day &&
                                    temp2->hour == hour && temp2->minute == minutes) {
                                    cout<< "\nThe meeting cannot be assigned because this teacher already has a meeting at the same"<<
                                    " time, day, month and year" << endl;
                                    invalide=false;
                                    break;
                                }
                                // check that a meeting is not scheduled when another is in progress
                                if (temp2->year == year && temp2->month == mounth && temp2->day == day &&
                                    temp2->hourEnd > hour) {
                                    cout<< "\nYou cannot schedule the meeting because you will be in another meeting at that time"<< endl;
                                    invalide=false;
                                    break;
                                }
                                // check that a meeting is not scheduled when another is in progress
                                if (temp2->year == year && temp2->month == mounth && temp2->day == day &&
                                    temp2->hourEnd == hour && temp2->minuteEnd > minuteE) {
                                    cout<< "\nYou cannot schedule the meeting because you will be in another meeting at that time"<< endl;
                                    invalide=false;
                                    break;
                                }
                                temp2 = temp2->next;
                            }
                            if(invalide== false) {
                                break;
                            }
                            temp=temp->next;
                        }
                        if(invalide== false) {
                            continue;
                        }
                        if (hourE < hour) {
                            cout << "\nThe end time must be greater than the start time" << endl;
                            continue;
                        } else if (hourE == hour && minuteE < minutes) {
                            cout << "\nThe end time must be greater than the start time" << endl;
                            continue;
                        }
                        //Here the date is valid
                        meet2->hour=hour, meet2->year=year, meet2->minute=minutes, meet2->day=day;
                        meet2->month=mounth, meet2->minuteEnd=minuteE, meet2->hourEnd=hourE, meet2->weeK=week, meet2->dayName=dayName;
                        cout<<"\nSuccessful modification"<<endl;
                        return;
                    } else {
                        cout << "Incorrect format: HHHH/DD/MM/AAAA";
                    }
                }
            }
        }
    }
}
void HardCode(){
    //add teacher
    addTeacher("Lorena Picado", 1, "Female");
    addTeacher("Abel Mendez", 2, "Male");
    addTeacher("Rocio Aguilar", 3, "Female");
    addTeacher("Vera Gamboa", 4, "Female");
    addTeacher("Karina Gonzales", 5, "Female");
    addTeacher("Oscar Viquez", 6, "Male");
    //add students
    addStudent("Alonso Martinez", 5, "Male");
    addStudent("Estefania Perez", 2, "Female");
    addStudent("Luz Mora", 3, "Female");
    addStudent("Bryam Lopez", 1, "Male");
    addStudent("Josue Orozco", 4, "Male");
    addStudent("Fernanda Chavez", 6, "Female");
    //add courses
    addCourse("Estructura de Datos", "CA1101", 3);
    addCourse("Programacion Orientada a Objetos", "CA1298", 3);
    addCourse("Calculo Diferencial e Integral", "MA1226", 4);
    addCourse("Matematicas Discretas", "MA7878", 4);
    addCourse("Arquitectura de Computadores", "CA1103", 4);
    addCourse("Introduccion a la Programacion", "CA1558", 3);
    //add admins
    addAdmin("Veronica Brenes", 1, "Female");
    addAdmin("Christian Gonzales", 2, "Male");
    addAdmin("Monica Speer", 3, "Female");
    addAdmin("Julio Cesar", 4, "Male");
    addAdmin("Sonia Rojas", 5, "Female");
    //Relate teacher to course
    relateTeacherCourse(5, "MA7878", 53);
    relateTeacherCourse(2, "CA1558", 50);
    relateTeacherCourse(1, "CA1101", 51);
    relateTeacherCourse(3, "CA1103", 52);
    relateTeacherCourse(4, "CA1298", 52);
    relateTeacherCourse(5, "MA1226", 51);
    relateTeacherCourse(5, "MA1226", 53);
    //Relate student to course
    relateStudentCourse(5, "MA1226", 51);
    relateStudentCourse(5, "CA1101", 51);
    relateStudentCourse(2, "CA1101", 51);
    relateStudentCourse(2, "MA7878", 53);
    relateStudentCourse(2, "MA1226", 51);
    relateStudentCourse(3, "CA1558", 50);
    relateStudentCourse(4, "CA1101", 51);
    relateStudentCourse(1, "CA1103", 52);
    relateStudentCourse(1, "CA1298", 52);
    relateStudentCourse(1, "MA1226", 51);
    //Assign meeting to a teacher
    meetingTeacher(5, "MA1226",51 ,1,  13, 10, 16, 55,16, 04, 2021, 8,"Friday", "clase magistral");
    meetingTeacher(5, "MA1226",51,2,  16, 55, 18, 30,22, 04, 2021, 9,"Thursday",  "clase magistral");
    meetingTeacher(5, "MA1226",53,7,  15, 10, 19, 30,23, 04, 2021,9,"Friday",  "clase magistral");
    meetingTeacher(4, "CA1298",52,3,  7, 50, 11, 30, 29, 03, 2021,10,"Monday",  "clase magistral");
    meetingTeacher(3, "CA1103",52 ,4,  7, 50, 11, 30, 26, 03, 2021,6,"Friday",  "clase magistral");
    meetingTeacher(2, "CA1558",50 ,5,  7, 50, 11, 30, 26, 03, 2021,6,"Friday", "clase magistral");
    meetingTeacher(1, "CA1101",51 ,6,  7, 50, 11, 30, 26, 03, 2021,6,"Friday",  "clase magistral");
    meetingTeacher(5, "MA1226",51 ,8,  13, 10, 16, 55,18, 03, 2021,5,"Thursday",  "clase magistral");
    meetingTeacher(5, "MA1226",51,9,  16, 55, 18, 30,22, 03, 2021,6,"Monday",  "clase magistral");
    meetingTeacher(1, "CA1101",51,10,  15, 10, 19, 30,23, 04, 2021,9,"Friday",  "clase magistral");
    meetingTeacher(5, "MA1226",51,11,  7, 50, 11, 30, 10, 03, 2021,4,"Wednesday",  "clase magistral");
    meetingTeacher(5, "MA1226",51,17,  7, 50, 11, 30, 11, 03, 2021,4,"Wednesday",  "clase magistral");
    meetingTeacher(5, "MA1226",51,18,  7, 50, 11, 30, 12, 03, 2021,4,"Wednesday",  "clase magistral");
    meetingTeacher(5, "MA7878",53,19,  7, 50, 11, 30, 01, 03, 2021,4,"Wednesday",  "clase magistral");
    meetingTeacher(1, "CA1101",51,12,  7, 00, 9, 00, 13, 03, 2021,7,"Tuesday",  "clase magistral");
    meetingTeacher(1, "CA1101",51,13,  9, 10, 11, 30, 13, 03, 2021,7,"Tuesday",  "clase magistral");
    meetingTeacher(1, "CA1101",51,14,  12, 00, 13, 00, 13, 04, 2021,7,"Tuesday",  "clase magistral");
    meetingTeacher(5, "MA1226",51,15,  7, 55, 9, 30,13, 04, 2021, 7,"Tuesday",  "clase magistral");
    meetingTeacher(5, "MA1226",51,16,  13, 40, 16, 30,13, 04, 2021, 7,"Tuesday",  "clase magistral");
    //Students indicate meeting in which they participated
    meetingStudent(5, "MA1226", 51, 8);
    meetingStudent(5, "MA1226", 51, 9);
    meetingStudent(1, "MA1226", 51, 9);
    meetingStudent(1, "MA1226", 51, 8);
    meetingStudent (3, "CA1558", 50, 5);
    meetingStudent(2, "CA1101", 51, 13);
    meetingStudent(2, "CA1101", 51, 6);
    meetingStudent(2, "CA1101", 51, 12);
}
void viewNextMeetings(Teachers*teacher) {
    //See the meetings you have scheduled for the next week
    int dayInit=dy;
    int mounthM=mth;
    int yearM=yr;
    //Compare the day entered by the user with the days
    // of the week to know how much to initialize dayInit
    if(dayyName=="Monday")
        dayInit+=7;
    else if(dayyName=="Tuesday")
        dayInit+=6;
    else if(dayyName=="Wednesday")
        dayInit+=5;
    else if(dayyName=="Thursday")
        dayInit+=4;
    else if(dayyName=="Friday")
        dayInit+=3;
    else if(dayyName=="Saturday")
        dayInit+=2;
    else if(dayyName=="Sunday")
        dayInit+=1;
    //If dayInit is greater than 31 then it must be reset to 1
    // change the month and, if applicable, the year.
    if(dayInit>31) {
        dayInit = dayInit - 31;
        mounthM = mounthM + 1;
        if (mounthM > 12) {
            mounthM = mounthM - 12;
            yearM = yearM+1;
        }
    }
    int cont=0;
    string daysWeek[5]={"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
    cout<<"\n\tThis are the meetings for the next week:\n";
    while(cont<=5){
        LinkTeacher*temp=teacher->link;
        //Days are printed in order
        cout<<"\n"<<daysWeek[cont]<<" "<< dayInit<<"/"<<mounthM<<"/"<<yearM<<":";
        bool print=false;
        while(temp!= NULL){
            //Scroll courses teacher list
            Meeting*temp2=temp->firstMeeting;
            while(temp2 != NULL){
                //Scroll meetings list
                //It is validated that the meeting is on the requested date
                if(temp2->year=yearM && temp2->month==mounthM && temp2->day==dayInit){
                    //print is changed to True, it means that there was at least one meeting
                    // on the requested day of the week
                    print=true;
                    cout<<"\n\tMeeting at "<<" "<<temp2->hour<<":"<<temp2->minute<<" of "<<temp->course->name<<" group "<<temp->group<<endl;
                }
                temp2=temp2->next;
            }
            temp=temp->next;
        }
        //if print is false
        if(!print)
            cout<<"\nThere aren't meetings scheduleds for these day"<<endl;
        //The day is increased to move on to the next
        dayInit++;
        //If dayInit is greater than 31 then it must be reset to 1
        // change the month and, if applicable, the year.
        if(dayInit>31) {
            dayInit = dayInit - 31;
            mounthM =mounthM + 1;
            if (mounthM > 12) {
                mounthM = mounthM - 12;
                yearM = yearM+1;
            }
        }
        cont++;
    }
}
void viewAssists(Teachers*teacher){
    //See the attendance in each of the meetings already held.
    LinkTeacher*temp=teacher->link;
    while(temp != NULL){
        //The list of links is scrolled.
        Meeting*temp2=temp->firstMeeting;
        while(temp2 != NULL){
            //The meeting list is scrolled.
            if(searchDate(temp2)==false) {//if the meeting has already passed.
                queue<string>ausentes;
                //Queue where absent people will be kept
                cout << "\nMeenting ID: "<<temp2->id<< " date: " << temp2->day << "/" << temp2->month << " at "
                     << temp2->hour << ":" << temp2->minute << " " << temp->course->name<<" group: "<<temp->group;
                cout << "\nattended:" << endl;
                Students*students = firstStudent;
                bool print=false;
                while (students != NULL) {
                    //The students list is scrolled.
                    bool asiste=false;
                    //It is assumed that the student has not participated.
                    LinkStudent*courses = students->link;
                    if(courses==NULL){
                        students=students->next;
                        continue;
                    }
                    while (courses != NULL){
                        //Student's course list is scrolled.
                        SublistAssistance*assistance = courses->sublistAssistance;
                        while (assistance != NULL){
                            //Course's meetings list is scrolled.
                            if (assistance->id == temp2->id) {
                                //If the ID of the attendance is equal to the ID of the meeting of the teacher, yes participated.
                                print=true;
                                asiste=true;
                                cout <<"\t"<<students->fullName << endl;
                            }
                            assistance = assistance->next;
                        }
                        if(courses->course->code==temp->course->code && courses->group==temp->group){
                            if(asiste==false){
                                //If he attends remains false it means that he did not participate, the absent student's name is added to the queue.
                                ausentes.push(students->fullName);
                            }
                        }
                        courses = courses->next;
                    }
                    students = students->next;
                }
                if(print==false){
                    //If print remains false, it means that no student participated in the meeting
                    // the message "Nobody attended" is printed.
                    cout<<"\n\tNobody attended"<<endl;
                }
                cout<<"\nNot attended:"<<endl;
                if(ausentes.size()!=0){
                    string name;
                    while(ausentes.size()!=0){
                        //Those on the stack are absent, therefore they are printed at this point.
                        if(name!=ausentes.front()) {
                            cout << "\t" << ausentes.front() << endl;
                        }
                        name=ausentes.front();
                        ausentes.pop();
                    }
                }else{
                    cout<<"\tAll attended"<<endl;
                }
            }
            temp2 = temp2->next;
        }
        temp=temp->next;
    }
}
queue<string> studentsParticipated(queue<int> reunionesEfectuadas, string code, int group){
    //Returns a queue with names of students who participated in a meeting.
    queue<string>studentsName;
    //the names of the students who participated in all the meetings will be stored in this queue.

    Students*students= firstStudent;
    //The list of students is copied to go through it.

    while(students != NULL){ //The list of students is scrolled in search of students with the enrolled course.

        LinkStudent*linkCourses= students->link;
        //The student's sublist of courses is copied to find the enrolled course.

        while(linkCourses != NULL){//The list of courses is scrolled to identify if it is the course that contains the meetings to be verified.
            queue<int> reunionesEfectuadas2;
            if(linkCourses->course->code == code && linkCourses->group == group){//Verifies if is the sough course.
                bool flag2=true;
                //If this flag is always true, it is if all the student's attendance id were found
                //if it changes to false the student misses at least 1 meeting
                while(reunionesEfectuadas.size() != 0){
                    SublistAssistance*assistance=linkCourses->sublistAssistance;
                    //A copy of the attendance list is made

                    bool flag3=false;
                    //this flag changes to true if found on stack id in student list
                    /*
                     * Here it is determined if the student participated or not to all the meetings of the course for that each ID
                     *meeting MUST be in the ID queue
                     */
                    int id=reunionesEfectuadas.front();
                    reunionesEfectuadas2.push(reunionesEfectuadas.front());
                    reunionesEfectuadas.pop();

                    while (assistance != NULL){
                        if(assistance->id == id){
                            flag3=true;
                            break;
                        }
                        assistance=assistance->next;
                    }
                    if(flag3 == false){
                        flag2=false;
                        break;
                    }

                }
                while(reunionesEfectuadas2.size() != 0){
                    reunionesEfectuadas.push(reunionesEfectuadas2.front());
                    reunionesEfectuadas2.pop();
                }
                if(flag2) {
                    studentsName.push(students->fullName);
                    //if it arrives here it is because it never entered the if above, therefore all the id were found.
                }
                break;
            }

            linkCourses=linkCourses->next;
        }
        students=students->next;
    }
    return studentsName;
}

void viewAllAssists(Teachers*teacher){
    //look which students attended all meetings by course.
    LinkTeacher*temp=teacher->link;

    while(temp != NULL){
        queue<int>reunionesEfectuadas;
        //In this queue the id of meetings already held will be stored, note that the queue is created for each course, FIFO.
        Meeting*temp2=temp->firstMeeting;
        //The meeting list is saved to a pointer for scrolling.
        while(temp2 != NULL){

            if(searchDate(temp2)==false){//It is verified that the meeting has already taken place.
                reunionesEfectuadas.push(temp2->id);
                //If the meeting has been held, it is added to the thing, note that the first element will be last
                //but as it is the tail, it will come out first.
            }
            temp2 = temp2->next;
        }
        cout<<"\nCourse: "<<temp->course->name<<" Group: "<<temp->group<<endl;

        if(reunionesEfectuadas.size()==0){
            cout<<"\tThis course has no meetings held"<<endl;
            temp=temp->next;
            continue;
        }
        queue<string> estWentAllMeetings = studentsParticipated(reunionesEfectuadas, temp->course->code, temp->group);
        //Returns the names of the students who participated in all the meetings, the queue is sent to compare
        //the meeting IDs, the course and group code to identify the student who has the course enrolled.
        if(estWentAllMeetings.size()==0){
            cout<<"\tAll students have been absent at least once"<<endl;
        }else{
            while(estWentAllMeetings.size() != 0){
                cout<<"\t"<<estWentAllMeetings.front()<<endl;
                estWentAllMeetings.pop();
            }
        }
        temp=temp->next;
    }
}
struct Percentage{
    //List of teachers and their attendance percentages. Attributes: full name, percentage.
    string fullName;
    int percentage;
    Percentage*next;
    Percentage(string fn, int P){
        fullName=fn;
        percentage = P;
        next=NULL;
    }
}*firstTpercentage;

void addTeacherPercentage(string fn, int P){
    //This function allows adding the teacher and the percentage of attendance to the structure created for it.
    //Receive the name of the teacher and their total attendance percentage.
    if(firstTpercentage == NULL){ //If the structure is empty, the new teacher and his percentage are added.
        Percentage*nn = new Percentage(fn, P);
        firstTpercentage = nn;
        return;
    }
    else{
        Percentage*nn = new Percentage(fn, P);
        nn->next=firstTpercentage;
        firstTpercentage=nn; //the new node becomes the pointer and the next one becomes the old node
        return;
    }
}
struct CoursePercentage{
    //List of Courses and their attendance percentages. Attributes: full name, percentage and number of meeting.
    string fullName;
    int percentage;
    int n_meeting;
    CoursePercentage*next;
    CoursePercentage(string fn, int P, int m){
        fullName=fn;
        percentage = P;
        n_meeting = m;
        next=NULL;
    }
}*firstCpercentage;
void addCoursePercentage(string fn, int P, int m){
    // This function allows adding the course, the percentage of total attendance and the number of meetings to the structure created for it.
    // Receive the name of the course, its total attendance percentage and the number of meetings
    if(firstCpercentage == NULL){ //If the structure is empty, the new course, its percentage and number of meetings are added as the new node
        CoursePercentage*nn = new CoursePercentage(fn, P, m);
        firstCpercentage = nn;
        return;
    }
    else{
        CoursePercentage*nn = new CoursePercentage(fn, P, m);
        nn->next = firstCpercentage;
        firstCpercentage=nn; //the new node becomes the pointer and the next one becomes the old node
        return;
    }
}
bool deleteCoursePercentage(string fn) {
    //This function allows you to delete courses from the structure created to save their attendance percentages.
    //This function receives the course name
    if (firstCpercentage == NULL) //Verify that the structure is not empty
        return false;
    else if (firstCpercentage->fullName == fn) { //It is verified if the first node is the course to be eliminated
        firstCpercentage = firstCpercentage->next;
        return true;
    } else {
        CoursePercentage *temp = firstCpercentage;
        CoursePercentage *tempAnt = firstCpercentage;
        while (temp != NULL) {
            //The structure is traversed looking for a match in the name
            if (temp->fullName == fn) {
                tempAnt->next = temp->next;
                return true;
            }
            tempAnt = temp;
            temp = temp->next;
        }
        if (temp == NULL) {
            return false;
        }
    }
    return false; //If it returns false it is because the name was not found
}
void TeachersPercentageAttendance(){
    Teachers*teacher = firstTeacher;
    while(teacher != NULL){
        //The teacher list  is scrolled.
        if(teacher->link != NULL)
            cout<<"**************************************************************************"<<endl;
            cout<<"TEACHER: "<<teacher->fullName;
            // The teacher's name is printed only if the link is not empty.
        queue<int>percentageteacher;
        //Queue where the percentage of attendance,
        //of each course and group of each teacher is saved.
        LinkTeacher*temp=teacher->link;
        while (temp != NULL){
            //The list of links is scrolled
            queue<int>percentage;
            // Queue where the attendance percentage of each teacher is saved.
            Meeting*temp2=temp->firstMeeting;
            if(temp2 != NULL){
                cout<<"  Course: "<<temp->course->name<<" Group: "<<temp->group<<endl;
                //The course and group name is printed only if the link is not empty.
            }
            while(temp2 != NULL){
                //The meeting list is scrolled
                int totalstudents = 0;
                //This variable stores the total number of students in a meeting
                int attendance = 0;
                //This variable saves the students who attended the meeting
                if (searchDate(temp2)== false){ //if the meeting has already passed
                    // The date and time are printed for each meeting
                    cout << "\nMeenting " << temp2->day << "/" << temp2->month << "/" << temp2->year << " at "
                         << temp2->hour << ":" << temp2->minute;
                    Students*students = firstStudent;
                    while(students != NULL){
                        //The students list is scrolled
                        bool asiste=false;
                        //It is assumed that the student has not participated
                        LinkStudent*courses = students->link;
                        if(courses==NULL){
                            //If the student link is empty, it is passed to the next student
                            students=students->next;
                            continue;
                        }
                        while (courses != NULL){
                            //Student's course list is scrolled
                            SublistAssistance*assistance = courses->sublistAssistance;

                            while (assistance != NULL){
                                //Course's meetings list is scrolled
                                if (assistance->id == temp2->id){
                                    //If the ID of the attendance is equal to the ID of the meeting of the teacher, yes participated
                                    asiste=true;
                                    attendance += 1;
                                    totalstudents += 1;
                                }
                                assistance = assistance->next;
                            }
                            if(courses->course->code==temp->course->code && courses->group==temp->group){
                                if(asiste == false){
                                    // If he attends remains false it means that he did not participate, 1 is added to the total of students
                                    totalstudents += 1;
                                }
                            }
                            courses = courses->next;
                        }
                        students = students->next;
                    }
                }
                //The attendance percentage for each meeting is defined, added to the queue
                if(attendance == 0) {
                    percentage.push(0);
                    cout << " 0%" << endl;
                }
                else if(attendance == totalstudents){
                    percentage.push(100);
                    cout<<" 100%"<<endl;
                }
                else{
                    float T = ((attendance + 0.0)/ totalstudents)*100;
                    percentage.push(T);
                    cout<<" "<<T<<"%"<<endl;

                }
                temp2 = temp2->next;
            }
            //Total stores the percentage of attendance by course and group
            int Total = 0;
            //The Amount variable stores the number of items in the queue, that is, the number of meetings per course and group
            int amount = percentage.size();
            if(percentage.size() != 0){
                while (percentage.size() != 0){
                    //Scroll the queue to add the percentages of the meetings
                    Total = Total + percentage.front();
                    //When adding an element it is eliminated from the queue
                    percentage.pop();
                }
                Total = (Total+0.0)/amount; // The percentage is obtained by course and group
                cout<<temp->course->name<<" group: "<<temp->group<<" has an attendance percentage of: "<<Total<<"%"<<endl<<endl;
                //The total attendance percentage of the course and group is added to the queue
                percentageteacher.push(Total);
            }
            temp = temp->next;
        }
        //The variable Amount2 stores the number of items in the queue, that is, the number of courses and group per teacher.
        int amount2 = percentageteacher.size();
        // Total2 stores the teacher's total attendance percentage
        int Total2 = 0;
        if(percentageteacher.size() != 0){
            while(percentageteacher.size()!= 0){
                //Scroll the queue to add the percentages of the courses
                Total2 = Total2 + percentageteacher.front();
                //When adding an element it is eliminated from the queue
                percentageteacher.pop();
            }
            Total2 = (Total2+0.0)/amount2;
            //The teacher's name and their attendance percentage are added to a simple structure
            addTeacherPercentage(teacher->fullName, Total2);
            cout<<"\n---- "<<teacher->fullName<<" has a Total attendance percentage of: "<<Total2<<"% ----"<<endl;
        }
        teacher = teacher->next;
    }
    Percentage*temp3 = firstTpercentage;
    //A string variable is created to store the teacher's name
    string teachers;
    while(temp3->next != NULL){
        //Se revisa la estructura de profesores y su porcentaje
       if(firstTpercentage->percentage > temp3->next->percentage){//The percentage of each teacher is compared to find the highest.
           //Being a loop, every time one is found to be greater than the next, the teacher's name will be saved,
           // until the structure is finished traversing.
           //The last to be saved will be the highest.
            teachers = firstTpercentage->fullName;
        }
        firstTpercentage = temp3->next;
        temp3 = firstTpercentage->next;
    }
    cout<<"**************************************************************************"<<endl;
    //Lastly, the teacher with the highest attendance percentage is printed
    cout<<endl<<teachers<<" HAS THE BEST ATTENDANCE PERCENTAGE"<<endl;
}
void WorstAttendancePercentage(){
    Teachers*teacher = firstTeacher;
    while(teacher != NULL) {
        //The teacher list  is scrolled.
        LinkTeacher*temp = teacher->link;
        while (temp != NULL) {
            //The list of links is scrolled
            queue<int>percentage;
            // Queue where the attendance percentage of each teacher is saved.
            Meeting*temp6=temp->firstMeeting;
                while (temp6 != NULL) {
                    //The meeting list is scrolled
                    int totalstudents = 0;
                    //This variable stores the total number of students in a meeting
                    int attendance = 0;
                    //This variable saves the students who attended the meeting
                    if (searchDate(temp6) == false) {//if the meeting has already passed
                        Students *students = firstStudent;
                        while (students != NULL) {
                            //The students list is scrolled
                            bool asiste = false;
                            //It is assumed that the student has not participated
                            LinkStudent *courses = students->link;
                            if (courses == NULL) {
                                //If the student link is empty, it is passed to the next student
                                students = students->next;
                                continue;
                            }
                            while (courses != NULL) {
                                //Student's course list is scrolled
                                SublistAssistance *assistance = courses->sublistAssistance;
                                while (assistance != NULL) {
                                    if (assistance->id == temp6->id) {
                                        //If the ID of the attendance is equal to the ID of the meeting of the teacher, yes participated
                                        asiste = true;
                                        attendance += 1;
                                        totalstudents += 1;
                                    }
                                    assistance = assistance->next;
                                }
                                if (courses->course->code == temp->course->code && courses->group == temp->group) {
                                    if (asiste == false) {
                                        // If he attends remains false it means that he did not participate, 1 is added to the total of students
                                        totalstudents += 1;
                                    }
                                }
                                courses = courses->next;
                            }
                            students = students->next;
                        }
                    }
                    //The attendance percentage for each meeting is defined and added to the queue
                    if (attendance == 0) {
                        percentage.push(0);
                    } else if (attendance == totalstudents) {
                        percentage.push(100);
                    } else {
                        float T = ((attendance + 0.0) / totalstudents) * 100;
                        percentage.push(T);
                    }
                    temp6 = temp6->next;
                }
                //Total stores the percentage of attendance by course and group
                int Total = 0;
                //The Amount variable stores the number of items in the queue, that is, the number of meetings per course and group
                int amount = percentage.size();
                if(amount>= 5){  // Verify that the course and group have at least 5 meetings held
                if (percentage.size() != 0) {
                    while (percentage.size() != 0) {
                        //Scroll the queue to add the percentages of the meetings
                        Total = Total + percentage.front();
                        //When adding an element it is eliminated from the queue
                        percentage.pop();
                    }
                    Total = (Total + 0.0) / amount; //The percentage of attendance per course is obtained
                    //The name of the course, the percentage of attendance and the number of meetings is added to a simple structure
                    addCoursePercentage(temp->course->name, Total, amount);
                }
                }
            temp = temp->next;
        }
        teacher = teacher->next;
    }
    // Contador
    int size1 = 3; //The number of courses with the lowest percentage of attendance to print
    int Per = 0; //Stores the percentage of the course
    string fullname; //Stores the course name
    int meeting; //Stores the number of meetings
    while( size1 != 0) {
        CoursePercentage *temp4 = firstCpercentage;
        if (temp4 == NULL && size1 == 3) {
            //Verify that the list is empty and that the counter remains at 3,
            // because if this happens there is no course that has at least 5 meetings held
            size1 = 0;
            cout << "There are no courses that have at least 5 meetings held" << endl;
            return;
        }
        else if (temp4->next == NULL) {
            //If there is only one course in the list, there is no one to compare it to.
            // Therefore that is the minor.
            Per = temp4->percentage;
            fullname = temp4->fullName;
            meeting = temp4->n_meeting;
            size1 = 0;
        }
        else {
            while (temp4 != NULL) {
                if(temp4->next== NULL){
                    Per = temp4->percentage;
                    fullname = temp4->fullName;
                    meeting = temp4->n_meeting;
                }
                else if (temp4->percentage < temp4->next->percentage) {
                    //Each percentage of one course is compared with another, looking for the lowest on the list.
                    // When found, the name, percentage and meeting data is saved.
                    Per = temp4->percentage;
                    fullname = temp4->fullName;
                    meeting = temp4->n_meeting;
                }
                temp4 = temp4->next;
            }
        }
        //Once the minor is found, it is removed from the structure,
        // so that when going through the list again,
        // the second minor is found and the third is following the same procedure.
        deleteCoursePercentage(fullname);
        //The course is printed with the percentages and the meetings held
        cout << fullname << " averages a " << Per << " % of attendance" <<" con "<<meeting<<" meetings held"<<endl;
        if (size1 != 0) {
            size1--;
        }
    }
}
Meeting * organizeMeeting(LinkTeacher*reuniones) {
    //The function returns an organized meeting list.
    Meeting *temp = reuniones->firstMeeting;
    Meeting *temp1 = temp;
    int size = 0;
    while (temp1!= NULL) {//Measures the size of the meeting list.
        size++;
        temp1 = temp1->next;
    }
    temp1 = temp;
    if(temp1==NULL){//If the list is NULL the method is not necessary, so it will return.
        return temp;
    }
    Meeting *temp2 = temp1->next;
    Meeting *temp3 = temp1;
    Meeting *temp4 = temp1;
    if(temp2==NULL){//If the list has just one meeting, the method is not necessary, so it will return.
        return temp;
    }
    temp1 = temp;
    while (true) { //The while will be running until the list is organized.
        int cont=2;
        if (temp1==temp) {//The while will be running until the list is organized.
            if ((temp2->month == temp->month && temp2->day < temp->day) || (temp2->month < temp->month)) {
                //If the first node is grater than the next node (by the month or day), the next node will be move
                //as the first one.
                reuniones->firstMeeting = temp2;
                temp=temp2;
                temp1->next=temp2->next;
                temp2->next=temp1;
                temp2=temp1;
                temp1=temp;
                temp3=temp1;
                temp4=temp1;
                continue;
            }
        }
        if ((temp1->month == temp2->month && temp1->day <= temp2->day) || (temp1->month < temp2->month)) {
            //If the node is not the first one or the last, but if it is grater than the next node (by the month or day), the next
            //node will be move one place back.
            temp1=temp2;
            temp2=temp2->next;
            temp3=temp1;
            temp4->next=temp1;
            while ((temp1->month == temp2->month && temp1->day <= temp2->day) || (temp1->month < temp2->month)) {
                //If the nodes are organized, the while will continue until the last node, if not, it means that it
                //is not organized and it has to restart with the current order.
                cont++;
                temp1 = temp2;
                temp2 = temp2->next;
                temp4=temp3;
                temp3=temp1;
                temp4->next=temp1;
                if (cont == size || (temp1->next==NULL && temp2==NULL))//If cont has the size of the meeting list,
                    //it means that the list is finally organized.
                    return temp;
            }
        }
        else if (temp1->month == temp2->month && temp1->day >= temp2->day) {
            //If the node is not the first one or the last, but if it is grater than the next node (by the day), the next
            //node will be move one place back.
            temp1->next = temp2->next;
            temp2->next = temp1;
            temp4->next = temp2;

            temp1 = temp;
            temp2 = temp1->next;
            temp3 = temp1;
            temp4 = temp1;
        } else if (temp1->month >= temp2->month) {
            //If the node is not the first one or the last, but if it is grater than the next node (by the month), the next
            //node will be move one place back.
            temp1->next = temp2->next;
            temp2->next = temp1;
            temp4->next = temp2;

            temp1 = temp;
            temp2 = temp1->next;
            temp3 = temp1;
            temp4 = temp1;
        }
    }
}
void attendanceMore3byCourse(Teachers*teacher) { //Report 4: Prints the students with three or more non-attendance attempts.
    LinkTeacher *temp = teacher->link;
    if (temp == NULL) {
        cout << "\nThe teacher doesn't have any courses assigned." << endl;
        return;
    }
    temp = teacher->link;
    while (temp != NULL) { // Goes through the list of courses.
        cout<<"\n-------"<<temp->course->name<<", Group: "<<temp->group<<"-------"<<endl;
        bool noEnrolled=true;
        bool attendance=true;
        bool attendance1=false;
        Students *temp2 = firstStudent;
        Meeting *temp5 = organizeMeeting(temp);//Organize meeting list.
        Meeting *orden = temp5;
        int sizeMeetings = 0;
        while (orden!= NULL) {//Measures the size of the meeting list.
            if(searchDate(orden)==false)
                sizeMeetings++;
            orden = orden->next;
        }
        if(sizeMeetings<=2){//If the size the is less or two, it means that no student will have more or three non-attendance attempts.
            cout<<"\nThe course has less than 3 meetings assigned."<<endl;
            temp=temp->next;
            continue;
        }
        orden = temp5;
        while (temp2 != NULL) { // Goes through the list of students.
            queue<string> out;
            bool first=false;
            LinkStudent *temp3 = temp2->link;
            if (temp3 == NULL) { // Checks if the student has any course enrolled.
                temp2 = temp2->next; // If the sublist is null the student doesn't have any courses enrolled.
                continue;
            } else {
                while (temp3 != NULL) { // Goes through the LinkStudent sublist of the student.
                    Meeting*temp4 = temp5;
                    if (temp->course->code == temp3->course->code) { // Compares the course wrote with each one the student has.
                        // If the comparison happens, two pointer will be created.
                        // One for the professor's sublist Meeting.
                        // And another one for the student's sublist SublistAssistance.
                        noEnrolled=true;
                        while (temp4 != NULL) { // Goes meeting by meeting created by the professor.
                            SublistAssistance *temp6 = temp3->sublistAssistance; // And another one for the student's sublist SublistAssistance.
                            if(first== false && temp6==NULL){// If the student enters and the sublist is NULL, the method will be all
                                // the meeting because the student did not attended to any meeting. This if is place at the begging because
                                //sublist will be run entirely.
                                cout <<"\n"<< temp2->fullName << " has " << sizeMeetings
                                     << " non-attendance attempts in the following dates:"<<endl;
                                while (orden!= NULL) {// Goes through the organized meeting list.
                                    if(searchDate(orden)==false)// If the meeting was already held, the meeting will not be printed.
                                        cout << orden->day << "/" << orden->month << endl;
                                    orden=orden->next;
                                }
                                orden= temp5;
                                break;
                            }
                            if(temp6==NULL){// If the sublist was entirely run until the last node,
                                // the condition will work for if the queue has meeting inside. This conditional works
                                // if the while was totally run.
                                attendance=true;
                                orden= temp5;
                                if(sizeMeetings>=3){
                                    cout<<"\n"<< temp2->fullName << " has " <<sizeMeetings<<" non-attendance attempts in the following dates:"<<endl;
                                    while (orden!= NULL) {
                                        if(searchDate(orden)==false){// If the meeting was already held, the meeting will not be printed.
                                            cout << orden->day << "/" << orden->month << endl;
                                            orden = orden->next;
                                        }
                                        orden = orden->next;
                                    }
                                    break;
                                }
                            }
                            while (temp6 != NULL) { //Goes through the attendance list indicated by the student.
                                if (temp4->id == temp6->id) {// Checks if the student attended to the meeting according to
                                    // assistance sublist. If the student attended to the meeting, the queue will be
                                    //set empty if the student did not attend to more than then 3 meeting previously.
                                    attendance=true;
                                    if (out.size() < 3) {
                                        while (!out.empty()) {
                                            out.pop();
                                        }
                                        break;
                                    } else if (out.size() >= 3) {//If the student did not attend to more than 3,
                                        //they will be printed because the next meeting the student did attend to it.
                                        attendance1=true;
                                        cout <<"\n"<< temp2->fullName << " has " << out.size()
                                             << " non-attendance attempts in the following dates:"<<endl;
                                        while (!out.empty()) {
                                            if(searchDate(temp4)==false){// If the meeting was already held, the meeting
                                                // will inserted in the queue.
                                                cout << out.front() << endl;
                                                out.pop();
                                            }
                                        }
                                        break;
                                    }
                                }
                                temp6 = temp6->next;
                            }
                            if (temp6!=NULL){//If the sublist is not NULL and the student attended to the meeting, the
                                //method will continue to the next meeting.
                                if(temp4->id == temp6->id){
                                    temp4 = temp4->next;
                                    continue;
                                }
                            }
                            if(searchDate(temp4)==false)//If the method get to this if, means that the student did not
                                //attend to the meeting. Therefore the meeting will be storage in the queue.
                                out.push(to_string(temp4->day) + "/" + to_string(temp4->month));
                            if (temp4->next == NULL && out.size() >= 3) {
                                attendance1=true;//If the meeting pointer points to NULL and the queue has a size of 3
                                //or more, the method prints the queue because the while is in its last run. Thus,
                                //if the queue must be printed.
                                cout <<"\n"<< temp2->fullName << " has " << out.size()
                                     << " non-attendance attempts in the following dates:"<<endl;
                                while (!out.empty()) {
                                    if(searchDate(temp4)==false){
                                        cout << out.front() << endl;
                                        out.pop();
                                    }
                                }
                            }
                            temp4 = temp4->next;
                        }
                    }
                    temp3 = temp3->next;
                }
            }
            temp2 = temp2->next;
        }
        if (attendance==true && attendance1==false){
            cout << "\nThere are no students that did not attended 3 times or more consecutively." << endl;
        }
        if (noEnrolled==false){
            cout << "\nThe course does not have students enrolled." << endl;
        }
        temp = temp->next;
    }
    if(temp==NULL)
        return;
    cout << "\nWrong name. Please rewrite the course's name properly." << endl;
}
void noAttendance(Teachers*teacher) { //Report 6: Prints if a course had no attendance. If the course does not have any
    //attendance the method prints the name and extra info about the course and meeting. If the course had attendance
    //the method notifies the user.
    LinkTeacher *temp = teacher->link;
    bool attended = false;//This variable is a flag to know if the student attended to the class, if the student does,
    //the flag is true and the course is not printed.
    bool oneAttended =true; //If the flag is true, the course had attendance in every meeting.
    if (temp == NULL) {// Checks if the teacher has any course assigned.
        cout << "\nThe teacher doesn't have any courses assigned." << endl;
        return;
    }
    while (temp != NULL) {//Goes through the list of courses.
        Meeting *temp1 = organizeMeeting(temp);
        while (temp1 != NULL) {//Goes through the list of meetings.
            Students *temp2 = firstStudent;
            while (temp2 != NULL) {//Goes through the list of student.
                if (attended) {//Breaks the while if a student attended a meeting of the course to jump to the next student.
                    temp2 = temp2->next;
                    break;
                }
                LinkStudent *temp3 = temp2->link;
                if (temp2->link != NULL) {// Checks if the student has any course enrolled.
                    while (temp3 != NULL) {//Goes through the group sublist of the student.
                        SublistAssistance *temp4 = temp3->sublistAssistance;
                        while (temp4 != NULL) {//Goes through the attendance list indicated by the student.
                            if (temp1->id == temp4->id) {//Breaks the while if a student attended a meeting of the course.
                                attended = true;
                                break;
                            }
                            temp4 = temp4->next;
                        }
                        if (attended)//Breaks the while if a student attended a meeting of the course.
                            break;
                        temp3 = temp3->next;
                    }
                    if (attended)//Breaks the while if a student attended a meeting of the course.
                        continue;
                }
                temp2 = temp2->next;
            }
            if (attended == false){//Prints the the flag is false which means that the meeting did not have any attendance.
                if(searchDate(temp1)==false){//Prints is the meeting was already done.
                    cout << "\n" << temp->course->name << " had no attendance on " << temp1->dayName << ": "
                         << temp1->day << "/" << temp1->month <<", starting at "<<temp1->hour<<":"<<temp1->minute<<"."<<endl;
                    oneAttended=false;//The flag is flag if the meeting had no attendance, which means that the course
                    //had no attendance so the method will not print that the course had attendance.
                }
            }
            temp1 = temp1->next;
            attended = false;
        }
        if(oneAttended){//If the flag is true the course had at least one attendance.
            cout << "\n" <<"The course " << temp->course->name<<" had attendance. " <<endl;
        }
        temp = temp->next;
    }
}

MeetingSpecificWeek*searchMeeting(string day, int week, LinkStudent*coursesStudent){
    //Function that returns a list with meetings of the same day of a specific student.
    MeetingSpecificWeek*meetings=NULL;
    while(coursesStudent != NULL){
        //Scroll courses student list.
        Teachers*teachers=firstTeacher;
        while(teachers != NULL){
            //Scroll teachers list.
            LinkTeacher*coursesTeacher=teachers->link;
            while(coursesTeacher != NULL){
                //Scroll courses teacher list.
                //If the course teacher is correct.
                if(coursesTeacher->course->code==coursesStudent->course->code && coursesTeacher->group==coursesStudent->group) {
                    Meeting*meetingsTeacher = coursesTeacher->firstMeeting;
                    while (meetingsTeacher != NULL) {
                        //Scroll meetings courses list.
                        //If the meeting is correct.
                        if (meetingsTeacher->weeK == week && meetingsTeacher->dayName == day) {
                            //If is NULL.
                            if(meetings==NULL){
                               meetings=new MeetingSpecificWeek(meetingsTeacher->id, meetingsTeacher->hour,
                               meetingsTeacher->minute, meetingsTeacher->hourEnd, meetingsTeacher->minuteEnd, meetingsTeacher->day,
                               meetingsTeacher->month, meetingsTeacher->year, meetingsTeacher->weeK, meetingsTeacher->dayName, meetingsTeacher->meetingTitle);
                           }
                            //If isn´t NULL.
                            else{
                               MeetingSpecificWeek*nM=new MeetingSpecificWeek(meetingsTeacher->id, meetingsTeacher->hour,
                               meetingsTeacher->minute, meetingsTeacher->hourEnd, meetingsTeacher->minuteEnd, meetingsTeacher->day,
                               meetingsTeacher->month, meetingsTeacher->year, meetingsTeacher->weeK, meetingsTeacher->dayName, meetingsTeacher->meetingTitle);
                               nM->next=meetings;
                               meetings=nM;
                           }
                        }
                        meetingsTeacher=meetingsTeacher->next;
                    }
                }
                coursesTeacher=coursesTeacher->next;
            }
            teachers=teachers->next;
        }
        coursesStudent=coursesStudent->next;
    }
    return meetings;
}
string nameCourse(int idMeeting){
    //Function that returns the name of a course.
    Teachers*teachers=firstTeacher;
    while(teachers != NULL){
        //Scroll teachers list.
        LinkTeacher*coursesTeacher=teachers->link;
        while(coursesTeacher != NULL){
            //Scroll courses list.
            Meeting*meetingsTeacher = coursesTeacher->firstMeeting;
            while (meetingsTeacher != NULL) {
                //Scroll meeting list.
                if (meetingsTeacher->id==idMeeting) {
                    return coursesTeacher->course->name;
                }
                meetingsTeacher=meetingsTeacher->next;
            }
            coursesTeacher=coursesTeacher->next;
        }
        teachers=teachers->next;
    }
    return "";
}
void scheduleOverlap(int id) { //report 5
    Students*student= searchStudent(id);
    LinkStudent *temp = student->link;
    //if temp==NULL there aren't courses
    if (temp == NULL) {
        cout << "The doesn't have any course enrolled." << endl;
        return;
    }
    //This variable save the option entered by the user
    int selected;
    while (true) {
        cout << "\nType the number of the week you want to check:" << endl;
        cout << "Or type 0 to go back." << endl;
        cin >> selected;
        //Verify that the entered week is in the allowed range
        if (selected > 18) {
            cout << "The week's number is out the academic week range." << endl;
            continue;
        } else if (selected == 0)
            break;
        //Verify that the entered week is in the allowed range
        else if (selected < 0) {
            cout << "The number is negative. Please do not write number under zero." << endl;
            continue;
        }else{
            //The necessary meetings and lists are saved divided into the days of the week.
            MeetingSpecificWeek*lunes=searchMeeting("Monday", selected, student->link);
            MeetingSpecificWeek*martes=searchMeeting("Tuesday", selected, student->link);
            MeetingSpecificWeek*miercoles=searchMeeting("Wednesday", selected, student->link);
            MeetingSpecificWeek*jueves=searchMeeting("Thursday", selected, student->link);
            MeetingSpecificWeek*viernes=searchMeeting("Friday", selected, student->link);
            //Sort the meetings

            queue<MeetingSpecificWeek*>days;
            //The lists are entered into the queue
            days.push(lunes);
            days.push(martes);
            days.push(miercoles);
            days.push(jueves);
            days.push(viernes);
            string daysWeek[5]={"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
            int cont=0;
            //While the queue is not empty
            while(!days.empty()){
                //Print the weekday
                cout<<"\nDay: "<<daysWeek[cont]<<endl;
                //Get queue list
                MeetingSpecificWeek*day=days.front();
                days.pop();
                //These two queues go to the meetings where there are and there are not clashes respectively.
                queue<MeetingSpecificWeek*>choque;
                queue<MeetingSpecificWeek*>noChoque;

                //If the list is empty it is because there were no meetings that day of the week.
                if(day==NULL){
                    cout<<"\tThere are not scheduled meetings for this day."<<endl;
                }
                //If the next one in the list is empty it is because there is only one meeting.
                else if (day->next==NULL){
                    cout<< "Meeting of "<< day->hour<<":"<<day->minute<<" at "<<day->hourEnd<<":"<<day->minuteEnd<< " class of "<< nameCourse(day->id)<<endl;
                }
                else{
                    int cont2=0;
                    MeetingSpecificWeek*temp=day;
                    while(day != NULL){
                        /*
                         * Basically the list is scrolled and in the other,
                         * cycle it is compared with the elements of the same.
                         * to identify clashes in the meetings of the same day.
                         */
                        //This bool verifies that a meeting is not repeated to the choque queue.
                        bool entra=false;
                        MeetingSpecificWeek*temp2=temp;
                        while(temp2 != NULL){
                            //Here begins the cycle to compare meetings of the same list in search of clashes.
                            //If it is the same meeting, the comparison is omitted.
                            if(day->id==temp2->id){
                                temp2=temp2->next;
                                continue;
                            }
                            if(day->hour > temp2->hour && day->hour < temp2->hourEnd){
                                //If the meeting has not entered the queue, enter.
                                if(entra==false) {
                                    choque.push(day);
                                    entra = true;
                                }
                            }
                            else if(day->hour == temp2->hour && day->hour < temp2->hourEnd && day->minute>= temp2->minute){
                                //If the meeting has not entered the queue, enter.
                                if(entra==false) {
                                    choque.push(day);
                                    entra = true;
                                }
                            }
                            else if(day->hour > temp2->hour && day->hour == temp2->hourEnd && day->minute <= temp2->minute){
                                //If the meeting has not entered the queue, enter.
                                if(entra==false) {
                                    choque.push(day);
                                    entra = true;
                                }
                            }
                            else if(day->hourEnd > temp2->hour && day->hourEnd == temp2->hourEnd && day->minuteEnd <= temp2->minuteEnd){
                                //If the meeting has not entered the queue, enter.
                                if(entra==false) {
                                    choque.push(day);
                                    entra = true;
                                }
                            }
                            else if(temp2->hour > day->hour && temp2->hour < day->hourEnd){
                                //If the meeting has not entered the queue, enter.
                                if(entra==false) {
                                    choque.push(day);
                                    entra = true;
                                }
                            }
                            temp2=temp2->next;
                        }
                        //If the meeting has not entered the queue, enter.
                        if(entra==false)
                            noChoque.push(day);
                        day=day->next;
                    }
                    //Meetings where there are clashes are printed.
                    if(!choque.empty()){
                        while(choque.size() != 0){

                            MeetingSpecificWeek*meet=choque.front();
                            choque.pop();
                            cout<< "\tMeeting of "<< meet->hour<<":"<<meet->minute<<" at "<<meet->hourEnd<<":"<<meet->minuteEnd<< " class of "<< nameCourse(meet->id)<<endl;
                        }
                        cout<<"\n\tHAY choque\n"<<endl;
                    }
                    //Meetings where there aren't clashes are printed.
                    if(!noChoque.empty()){
                        while(noChoque.size() != 0){
                            MeetingSpecificWeek*meet=noChoque.front();
                            noChoque.pop();
                            string nameC=nameCourse(meet->id);
                            cout<< "\tMeeting of "<< meet->hour<<":"<<meet->minute<<" at "<<meet->hourEnd<<":"<<meet->minuteEnd<< " class of "<< nameC<<endl;
                        }
                    }

                }
                cont++;
            }
        }

    }
}
bool PrintTeachersWoCourses(){
    // This function allows you to print all the professors entered with their respective ID if they do not have courses.
    Teachers*temp = firstTeacher;
    int contador = 0;
    while(temp != NULL){ // Each node of the structure is traversed and the name,
        // and ID of each professor is printed if he does not have assigned courses.
        if(temp->link == NULL){
            contador += 1;
            cout<<"Teacher's name: "<<temp->fullName<<" ID: "<<temp->id<<endl;
        }
        temp = temp->next;
    }
    if(contador == 0) {
        cout << "There are no teachers who do not have assigned courses"<<endl;
        return false;
    }
    return true;
}
void PrintTeachersWithCourses(){
    // This function allows you to print all the professors entered with their respective ID if they have courses.
    Teachers*temp = firstTeacher;
    while(temp != NULL){ // Each node of the structure is traversed and the name,
        // and ID of each teacher is printed if they have assigned courses.
        if(temp->link != NULL)
            cout<<"Teacher's name: "<<temp->fullName<<" ID: "<<temp->id<<endl;
        temp = temp->next;
    }
}
bool PrintStudentsWoCourses(){
    // This function allows to print all the students entered with their respective ID, if they do not have courses.
    Students*temp = firstStudent;
    int contador = 0;
    while(temp != NULL){ //Each node of the structure is traversed and the name and ID of each student is printed,
        // if he does not have assigned courses.
        if(temp->link ==NULL) {
            contador += 1;
            cout << "Student's name: " << temp->fullName << " ID: " << temp->id << endl;
        }
        temp = temp->next;
    }
    if(contador == 0) {
        cout << "There are not students who do not have assigned courses"<<endl;
        return false;
    }
    return true;
}
void PrintStudentsWithCourses(){
    // This function allows to print all the students entered with their respective ID, if they have courses.
    Students*temp = firstStudent;
    while(temp != NULL){ //Each node of the structure is traversed and the name and ID of each student is printed,
        // if they have assigned courses.
        if(temp->link != NULL)
        cout<<"Student's name: "<<temp->fullName<<" ID: "<<temp->id<<endl;
        temp = temp->next;
    }
}
void PrintedCourses(){
    // This function allows to print all the courses entered with their respective code.
    Courses*temp = firstCourse;
    do{ //Each node of the structure is traversed and the name and code of each course is printed.
        cout<<"Course's name: "<<temp->name<<" Code: "<<temp->code<<endl;
        temp = temp->next;
    }while(temp != firstCourse);

}
void PrintedTeacherInfo(Teachers*teacher){
    //This function shows the teacher the codes and groups of courses taught.
    //This function receives the teacher node.
    LinkTeacher*temp = teacher->link;
    while(temp != NULL){ //Go through each teacher link to print the code and group information for each course.
        cout<<"Course's name: "<<temp->course->name<<" Code: "<<temp->course->code<<" Group: "<<temp->group<<endl;
        temp = temp->next;
    }
}
void PrintedPossibleOptions(){
    //This function goes through the teachers and their link to print the code and group of the course they teach.
    //In order to print the information to the administrator and that he can see with which courses he can relate the student.
    Teachers*temp = firstTeacher;
    while(temp!= NULL){
        LinkTeacher*temp1 = temp->link;
        while(temp1 != NULL){
            cout<<"Course's name: "<<temp1->course->name<<" Code: "<<temp1->course->code<<" Group: "<<temp1->group<<endl;
            temp1 = temp1->next;
        }
        temp = temp->next;
    }
}
void PrintedIDMeetings(Students*student){
    //This function prints the student's courses, the course code, and the course group.
    // As well as the meetings that have been held. ID and date of each one.
    //Receive the student node.
    LinkStudent*temp = student->link;
    if(temp == NULL) //Verify that the student has links.
        cout<<"The student does not have enrolled courses"<<endl;
    while(temp != NULL){ //Scroll through the student links to find the course and group code.
        Teachers*teacher = firstTeacher;
        while(teacher != NULL){ //Scroll through the list of teachers.
            LinkTeacher*temp1 = teacher->link;
            while(temp1 != NULL){ //Go through the links of each teacher.
                if(temp1->course->code == temp->course->code && temp1->group == temp->group){ //Check if the code of the course and student group,
                    // and the code of the course and teacher group match.
                    Meeting*temp3 = temp1->firstMeeting;
                    if(temp3 != NULL){ //Verify that there are meetings to print courses that have information.
                        cout<<"\n Information on the course to which it belongs and the meetings that have been held"<<endl;
                        cout<<"Course's name: "<<temp->course->name<<" Code: "<<temp->course->code<<" Group: "<<temp->group<<endl;
                    }
                    while(temp3 != NULL){ //Scroll through the course meeting list.
                        if (searchDate(temp3)== false){ //Verify that the meeting has already been held to print the information.
                            cout << "\nMeenting: "<<" ID: " <<temp3->id<< " (" <<temp3->day << "/" << temp3->month << "/" << temp3->year << " at "
                                 << temp3->hour << ":" << temp3->minute<<")"<<endl;
                        }
                        temp3 = temp3->next;
                    }
                }
                temp1 = temp1->next;
            }
            teacher = teacher->next;
        }
        temp = temp->next;
    }
}
void menu(){ //This feature allows the user to navigate and interact with the program effectively.

    while(true) { //Shows the user the three sections they have access to.
        cout << "\n--------Option Menu--------" << endl;
        cout << "\nType -1- for Teachers." << endl;
        cout << "Type -2- for Students." << endl;
        cout << "Type -3- for Administrators." << endl;
        cout << "Type -4- for EXIT." << endl;
        int num;
        cout << "\nType the number on your keyboard of the option you want to execute: ";
        cin >> num;
        //Variables are declared that will be used in various menu functions regardless of the section.
        string codeCourse;
        int group;
        int idMeeting;
        if (num == 1) {
            int idTeacher;
            cout << "\nThe teacher ID by default is: 1\nType your ID Teacher: ";
            cin >> idTeacher;
            Teachers*teacher = searchTeacher(idTeacher);//It is verified that the ID entered belongs to a teacher on the platform.
            if (teacher != NULL) { // if it is incorrect, access will be denied.
                while(true) {
                    cout << "\n-------WELCOME TO THE TEACHER PLATFORM.--------" << endl;
                    cout << "\nTeacher: "<<teacher->fullName<< endl;
                    cout << "\nType -1- To insert a meeting to a course." << endl;
                    cout << "Type -2- To modify a course meeting." << endl;
                    cout << "Type -3- To delete a course meeting." << endl;
                    cout << "Type -4- View meetings for next week." << endl; //Report 1.
                    cout << "Type -5- View attendance of meetings already held." << endl; //Report 2.
                    cout << "Type -6- View which students attended all meetings by course." << endl; //Report 3.
                    cout << "Type -7- To check students that didn't attend more than 3 times." << endl; //Report 4.
                    cout << "Type -8- To check if a course did not have attendance." << endl; //Report 6.
                    cout << "Type -9- To go back to main menu." << endl;
                    string optionTeacher;
                    cout << "\nType the option: ";
                    cin >> optionTeacher;
                    if (optionTeacher == "1") {
                        if(teacher->link==NULL){
                            cout<<"\nThis teacher has no assigned courses."<<endl;
                            continue;
                        }
                        int hour;
                        int minute;
                        int hourE;
                        int minuteE;
                        int day;
                        int month;
                        int year;
                        int week;
                        string dayName;
                        string titleMeeting;
                        cout<<"Information on courses taught:"<<endl;
                        PrintedTeacherInfo(teacher); //Allows you to view the courses taught by the teacher and their information.
                        cout << "\nEnter the code of the Course class to be scheduled: ";
                        cin >> codeCourse;
                        cout << "\nEnter the group of the Course class to be scheduled: ";
                        cin >> group;
                        cout << "\nEnter the Meeting ID of the class to be scheduled: ";
                        cin >> idMeeting;
                        while (true) {
                            cout << "\nEnter the name of the day of the meeting will be held (From Monday to Friday): "<<endl;
                            cin>>dayName;
                            cout << "\nEnter the number of the week the meeting will be held: "<<endl;
                            cin>>week;
                            cout << "\nEnter the date Meeting of the class to be scheduled (example 18:10/11/05/2021): "<<endl;
                            cin >> hour >> c >> minute >> d >> day >> a >> month >> b >> year;
                            if (hour >= 0 && hour <= 23 && c == ':' && minute >= 0 && minute <= 59 && d == '/' &&
                                day > 0 && day < 32 && a == '/' && month > 0 && month < 13 && b == '/' &&
                                year > 2020 && year < 2022) {
                                cout << "\nEnter the date meeting of the class to be scheduled (example 18:10): ";////
                                cin >> hourE >> c >> minuteE;
                                if (hourE >= 0 && hourE <= 23 && c == ':' && minuteE >= 0 && minuteE <= 59) {
                                    break;
                                }
                            } else {
                                cout << "\nWrong Format: HHHH/DD/MM/AAAA";
                            }
                        }
                        cout << "\nEnter a name for the meeting to be scheduled: ";
                        cin.ignore();
                        getline(cin, titleMeeting);
                        bool meeting = meetingTeacher(idTeacher, codeCourse,group, idMeeting, hour, minute, hourE, minuteE, day, month, year, week,dayName, titleMeeting);
                        if (meeting)
                            cout << "\nThe meeting was inserted successfully." << endl;
                        else
                            cout << "\nERROR! The meeting was not inserted." << endl;
                    }else if (optionTeacher == "2") {
                        if(teacher->link==NULL){
                            cout<<"\nThis teacher has no assigned courses."<<endl;
                            continue;
                        }
                        cout<<"Information on courses taught:"<<endl;
                        PrintedTeacherInfo(teacher); //Allows you to view the courses taught by the teacher and their information.
                        cout << "\nEnter the course code: ";
                        cin >> codeCourse;
                        cout << "\nEnter the group number: ";
                        cin >> group;
                        modifyMeetingOfTeacher(idTeacher, codeCourse, group);
                    } else if (optionTeacher == "3") {
                        if(teacher->link==NULL){
                            cout<<"\nThis teacher has no assigned courses."<<endl;
                            continue;
                        }
                        cout<<"Information on courses taught:"<<endl;
                        PrintedTeacherInfo(teacher); //Allows you to view the courses taught by the teacher and their information.
                        cout << "\nEnter the course code: ";
                        cin >> codeCourse;
                        cout << "\nEnter the group number: ";
                        cin >> group;
                        deleteMeetingOfTeacher(idTeacher, codeCourse, group);
                    }else if (optionTeacher == "4") {
                        if(teacher->link==NULL){
                            cout<<"\nThis teacher has no assigned courses."<<endl;
                            continue;
                        }
                        viewNextMeetings(teacher);
                    }else if (optionTeacher == "5") {
                        if(teacher->link==NULL){
                            cout<<"\nThis teacher has no assigned courses."<<endl;
                            continue;
                        }
                        viewAssists(teacher);
                    }else if (optionTeacher == "6") {
                        if(teacher->link==NULL){
                            cout<<"\nThis teacher has no assigned courses."<<endl;
                            continue;
                        }
                        viewAllAssists(teacher);
                    }else if (optionTeacher == "7") {
                        if(teacher->link==NULL){
                            cout<<"\nThis teacher has no assigned courses."<<endl;
                            continue;
                        }
                        attendanceMore3byCourse(teacher);
                    }else if (optionTeacher == "8") {
                        if(teacher->link==NULL){
                            cout<<"\nThis teacher has no assigned courses."<<endl;
                            continue;
                        }
                        noAttendance(teacher);
                    }else if(optionTeacher == "9") {
                        break;
                    } else
                        cout << "ERROR! The typed option does not exist." << endl;
                }
            } else
                cout<<"Your teacher ID  is not registered on the platform." << endl;
        }
        else if (num == 2){
            int IDStudent;
            cout << "\nThe student ID by default is: 1\nType your ID Student: ";
            cin >> IDStudent;
            Students *student = searchStudent(IDStudent);//If it returns NULL the student does not exist,
            // but it already exists.
            if (student != NULL) {
                while(true) {
                    cout << "\n-------WELCOME TO THE STUDENT PLATFORM--------" << endl;
                    cout << "\nStudent: "<<student->fullName<< endl;
                    cout << "\nType -1- Course meeting attendance record." << endl;
                    cout << "Type -2- Check for schedule overlap." << endl; //report 5
                    cout << "Type -3- To go Back to Main Menu." << endl;
                    int OptionStudent;
                    cout << "\n Type the option: ";
                    cin >> OptionStudent;
                    if (OptionStudent == 1) {
                        if(student->link==NULL){
                            cout<<"\nThis student has no assigned courses."<<endl;
                            continue;
                        }
                        PrintedIDMeetings(student); //This function prints the student's courses, the course code, and the course group.
                        // As well as the meetings that have been held. ID and date of each one.
                        cout << "\nEnter the Course code: ";
                        cin >> codeCourse;
                        cout << "\nEnter the group number :";
                        cin >> group;
                        cout << "\nEnter the meeting's ID of the course: ";
                        cin >> idMeeting;
                        bool flag=meetingStudent(IDStudent, codeCourse, group, idMeeting);
                        if(flag)
                            cout<<"\nSuccessfully registered attendance"<<endl;
                    }else if(OptionStudent==2){
                        if(student->link==NULL){
                            cout<<"\nThis student has no assigned courses."<<endl;
                            continue;
                        }
                        scheduleOverlap(student->id);
                    }else if(OptionStudent==3)
                        break;
                    else
                        cout << "\nERROR! The typed option does not exist." << endl;
                }
            } else{
                cout<<"Your Student ID is not registered on the platform." << endl;
                return;}
        }
        else if (num == 3){
            int IDadm;
            cout<<"The admin ID by default is: 1\n\nType your Administrator ID: ";
            cin >> IDadm;
            Administrators*admin=searchAdmin(IDadm);//Verify that the administrator is registered on the platform.
            // If not, it will not be allowed to enter.
            if(admin != NULL){
                    while(true) {
                        cout << "\n--------WELCOME TO THE ADMINISTRATOR PLATFORM--------" << endl;
                        cout << "\nAdmin: "<<admin->fullName<< endl;
                        cout << "\nType -1- To perform actions on teachers with no assigned courses." << endl; //Insert, modify and delete teachers without courses.
                        cout << "Type -2- To perform actions on students with no assigned courses." << endl;//Insert, modify and delete students without courses.
                        cout << "Type -3- To perform actions on courses." << endl; //Insert, modify and delete courses.
                        cout << "Type -4- To perform actions on teachers with assigned courses." << endl;// Relate,modify and delete teachers with courses.
                        cout << "Type -5- To perform actions on students with assigned courses." << endl; //Relate, modify and delete students with courses.
                        cout << "Type -6- To check if a course did not have attendance." << endl; //Report 6.
                        cout << "Type -7- To check who is the teacher with the best percentage attendance." << endl; //Report 7.
                        cout << "Type -8- To check which are the three courses with the worst percentage attendance, which have at least 5 meetings held." << endl; // Report 8.
                        cout << "Type -9- To insert teachers." << endl;
                        cout << "Type -10- To insert teachers." << endl;
                        cout << "Type -11- To go back to main menu." << endl;
                        int Option;
                        cout << "\nType the number of the option you want to execute on your keyboard:" << endl;
                        cin >> Option;
                        //Global variables are declared for the administrators menu.
                        int Options;
                        string fullname;
                        int Id;
                        string gender;
                        if (Option == 1) {
                            while(true) {
                                cout << "\nType -1- To Delete teachers." << endl; //
                                cout << "Type -2- To Modify teachers with no assigned courses." << endl;
                                cout << "Type -3- To go back to main menu." << endl;
                                cout << "Type the letter of the option: ";
                                cin >> Options;
                                 if (Options == 1) {
                                    cout << "Teacher information:" << endl;
                                    bool answer = PrintTeachersWoCourses();
                                    if(answer== true) {
                                        cout << "Insert the teacher ID:" << endl;
                                        cin >> Id;
                                        bool result = deleteTeacherWoCourses(Id);
                                        if (result == true) {
                                            cout << "The teacher was deleted successfully." << endl;
                                        } else
                                            cout << "The teacher was not removed correctly." << endl;
                                    }
                                } else if (Options == 2) {
                                    cout << "Teacher information:" << endl;
                                    bool answer = PrintTeachersWoCourses();
                                    if(answer== true) {
                                        cout << "Insert the teacher ID:" << endl;
                                        cin >> Id;
                                        modifyTeacherWoCourses(Id);
                                    }
                                } else if (Options == 3) {
                                    break;
                                } else {
                                    cout << "ERROR!! The typed option does not exist." << endl;
                                    return;
                                }
                            }
                        } else if (Option == 2) {
                            while(true) {
                                cout << "\nType -1- To Insert students." << endl;
                                cout << "Type -2- To Delete students." << endl;
                                cout << "Type -3- To Modify students." << endl;
                                cout << "Type -4- To go back to main menu." << endl;
                                cout << "Type the letter of the option:" << endl;
                                cin >> Options;
                                if (Options == 1) {
                                    cout << "Insert the full name of the student." << endl;
                                    cin >> fullname;
                                    cout << "Insert the student ID." << endl;
                                    cin >> Id;
                                    cout << "Insert the gender of the student." << endl;
                                    cin >> gender;
                                    bool result = addStudent(fullname, Id, gender);
                                    if (result == true) {
                                        cout << "The student was added successfully." << endl;
                                    } else
                                        cout << "The student was not added correctly." << endl;
                                } else if (Options == 2) {
                                    cout << "Students Information:" << endl;
                                    bool answer = PrintStudentsWoCourses();
                                    if(answer == true) {
                                        cout << "Insert the Student ID:" << endl;
                                        cin >> Id;
                                        bool result = deleteStudentWoCourses(Id);
                                        if (result == true) {
                                            cout << "The student was deleted successfully." << endl;
                                        } else
                                            cout << "The student was not removed." << endl;
                                    }
                                } else if (Options == 3) {
                                    cout << "Students Information:" << endl;
                                    bool answer= PrintStudentsWoCourses();
                                    if(answer == true) {
                                        cout << "Insert the student ID:" << endl;
                                        cin >> Id;
                                        modifyStudentWoCourses(Id);
                                    }
                                } else if (Options == 4) {
                                    break;
                                } else {
                                    cout << "ERROR!! The typed option does not exist." << endl;
                                    return;
                                }
                            }
                        } else if (Option == 3) {
                            while(true) {
                                cout << "Type -1- To Delete courses." << endl;
                                cout << "Type -2- To Modify courses." << endl;
                                cout << "Type -3- To go back to main menu." << endl;
                                cout << "Type the letter of the option." << endl;
                                cin >> Options;
                                string code;
                                if (Options == 1) {
                                    cout << "Courses Information:" << endl;
                                    PrintedCourses();
                                    cout << "Insert the code of the course." << endl;
                                    cin >> code;
                                    bool result = deleteCourse(code);
                                    if (result == true) {
                                        cout << "The course was deleted successfully." << endl;
                                    } else
                                        cout << "The course was not removed correctly." << endl;

                                } else if (Options == 2) {
                                    cout << "Courses Information." << endl;
                                    PrintedCourses();
                                    cout << "Insert the code of the course." << endl;
                                    cin >> code;
                                    modifyCourse(code);
                                } else if (Options == 3) {
                                    break;
                                } else {
                                    cout << "ERROR! The typed option does not exist." << endl;
                                    return;
                                }
                            }
                        } else if (Option == 4) {
                            while (true) {
                                cout << "\nType -1- To Delete teachers with courses." << endl;
                                cout << "Type -2- To Modify teachers with courses." << endl;
                                cout << "Type -3- To Relate teachers with courses." << endl;
                                cout << "Type -4- To go back to main menu." << endl;
                                cout << "Type the letter of the option." << endl;
                                cin >> Options;
                                if (Options == 1) {
                                    cout << "Teachers Information:" << endl;
                                    PrintTeachersWithCourses();
                                    cout << "Insert the teacher ID." << endl;
                                    cin >> Id;
                                    deleteTeacherWithCourses(Id);
                                } else if (Options == 2) {
                                    cout << "Teachers Information:" << endl;
                                    PrintTeachersWithCourses();
                                    cout << "Insert the teacher ID." << endl;
                                    cin >> Id;
                                    modifyTeacherWithCourses(Id);
                                }else if(Options == 3){
                                    cout<<"Teacher information: "<<endl;
                                    PrintTeachersWithCourses();
                                    PrintTeachersWoCourses();
                                    cout<<"Courses information: "<<endl;
                                    PrintedCourses();
                                    cout << "Insert the teacher ID." << endl;
                                    cin >> Id;
                                    cout << "Insert the code course." << endl;
                                    cin>>codeCourse;
                                    cout << "The group is created by you here\n Insert the group: " << endl;
                                    cin>>group;
                                    bool answer = relateTeacherCourse(Id, codeCourse, group);
                                    if(answer == true){
                                        cout<<"The action has been successful"<<endl;
                                    }
                                    else{
                                        cout<<"The action has not been successful"<<endl;
                                    }
                                } else if (Options == 4) {
                                    break;
                                } else {
                                    cout << "ERROR! The typed option does not exist." << endl;
                                    return;
                                }
                            }
                        } else if (Option == 5) {
                            while (true) {
                                cout << "\nType -1- To Delete students with courses." << endl;
                                cout << "Type -2- To Modify students with courses." << endl;
                                cout << "Type -3- To Relate students with courses." << endl;
                                cout << "Type -4- To go back to main menu." << endl;
                                cout << "Type the letter of the option." << endl;
                                cin >> Options;
                                if (Options == 1) {
                                    cout << "Students Information:" << endl;
                                    PrintStudentsWithCourses();
                                    cout << "Insert the student ID." << endl;
                                    cin >> Id;
                                    deleteStudentWithCourses(Id);
                                } else if (Options == 2) {
                                    cout << "Students Information:" << endl;
                                    PrintStudentsWithCourses();
                                    cout << "Insert the student ID." << endl;
                                    cin >> Id;
                                    modifyStudentWithCourses(Id);
                                }else if(Options == 3){
                                    cout<<"Students information: "<<endl;
                                    PrintStudentsWithCourses();
                                    PrintStudentsWoCourses();
                                    cout<<"Courses information: "<<endl;
                                    PrintedPossibleOptions();
                                    cout << "Insert the student ID." << endl;
                                    cin >> Id;
                                    cout << "Insert the code course." << endl;
                                    cin>>codeCourse;
                                    cout << "Insert the group: " << endl;
                                    cin>>group;
                                    bool answer = relateStudentCourse(Id, codeCourse, group);
                                    if(answer == true){
                                        cout<<"The action has been successful"<<endl;
                                    }
                                    else{
                                        cout<<"The action has not been successful"<<endl;
                                    }
                                } else if (Options == 4) {
                                    break;
                                } else {
                                    cout << "ERROR! The typed option does not exist." << endl;
                                    return;
                                }
                            }
                        } else if (Option == 6) {
                            cout<<"Teachers Information:"<<endl;
                            PrintTeachersWithCourses();
                            cout << "Insert the teacher ID." << endl;
                            cin >> Id;
                            Teachers*professor= searchTeacher(Id);
                            noAttendance(professor);
                        } else if (Option == 7) {
                            TeachersPercentageAttendance();
                        }else if (Option == 8) {
                            cout<<"If only one or two courses are obtained, it is because there are no more courses that have a minimum of 5 meetings held."<<endl;
                            cout<<"--------------------------------------------------------------------------------------------------------"<<endl;
                            WorstAttendancePercentage();
                        }
                        else if (Option == 9) {
                            cout << "\nInsert the full name of the teacher." << endl;
                            cin.ignore();
                            getline(cin, fullname);
                            cout << "Insert the teacher ID:" << endl;
                            cin >> Id;
                            cout << "Insert the gender of the teacher." << endl;
                            cin >> gender;
                            bool result = addTeacher(fullname, Id, gender);
                            if (result == true) {
                                cout << "The teacher was added successfully." << endl;
                            } else
                                cout << "The teacher was not added correctly." << endl;
                        }
                        else if (Option == 10) {
                            string name;
                            string code;
                            int credits;
                            cout << "Insert the name of the course." << endl;
                            cin >> name;
                            cout << "Insert the code of the course." << endl;
                            cin >> code;
                            cout << "Insert the credits of the course." << endl;
                            cin >> credits;
                            bool result = addCourse(name, code, credits);
                            if (result == true) {
                                cout << "The course was added successfully." << endl;
                            } else
                                cout << "The course was not added correctly." << endl;
                        } else if (Option == 11) {
                            break;
                        } else {
                            cout << "ERROR! The typed option does not exist." << endl;
                            return;
                        }
                    }
            }
            else{
                cout << "\nYour Administrator ID is not registered on the platform." << endl;
                return menu();}
        }
        else if (num == 4){
            cout<<"\n***** Thanks for using our platform! *****"<<endl;
            break;
        }
    }
}

int main(){
    while(true) {
        //a date entered by the user
        hr=00;
        cout<<"Please, type your current time (format: HHHH/DD/MM/AAAA; e.g: 15:20/29/03/2021): " ;
        cin >> hr >> c >> mins >> d >> dy >> a >> mth >> b >> yr;
        if (hr >= 00 && hr <= 23 && c == ':' && mins >= 0 && mins <= 59 && d == '/' && dy > 0 && dy < 32 && a == '/' && mth > 0 && mth < 13 && b == '/' && yr > 2020 && yr < 2022) {
            cout << "\nThe date is correct." << endl;
            while (true) {
                cout<<"\n\nPlease, type the day Name (e.g: Monday): " ;
                cin>>dayyName;
                if (dayyName == "Monday" || dayyName == "Tuesday" || dayyName == "Wednesday" ||
                    dayyName == "Thursday" || dayyName == "Friday" || dayyName == "Saturday" || dayyName == "Sunday") {
                    cout << "\nThe day is correct." << endl;
                    break;
                } else {
                    cout << "Wrong Format: Monday " << endl;
                    continue;
                }
            }
            break;
        }
        else{
            cout << "Wrong Format: HHHH/DD/MM/AAAA"<<endl;
            continue;
        }
    }
    HardCode();
    menu();
    return 0;
}